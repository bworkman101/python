import Image
import ImageDraw
import logging
import math


class Scanner:
    min_scan_angle = math.pi / 12.0
    max_scan_angle = math.pi - math.pi / 12.0
    max_tangent = 253
    scan_step_angle = 0.01

    def __init__(self, floor_img='floor.jpeg'):

        # LOGGER
        self.logger = logging.getLogger('Scanner')
        FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        logging.basicConfig(format=FORMAT)
        self.logger.setLevel(logging.DEBUG)

        self.logger.info("initializing world " + floor_img)
        self.world_image_img = Image.open(floor_img)
        self.world_image = self.world_image_img.convert('RGB')

    def scan_world(self, robot, landmarks):
        self.logger.info("scanning world")

        landmarks.reset()

        scan_angle = self.min_scan_angle

        while scan_angle < self.max_scan_angle:
            scan_coordinate = self._scan_tangent(scan_angle, robot)

            if scan_coordinate is not False:
                landmarks.add(scan_coordinate)

            scan_angle += self.scan_step_angle

    def _scan_tangent(self, angle, robot):

        scan_angle = angle - robot.orientation

        for t in range(self.max_tangent):

            relative_x = float(t) * math.sin(scan_angle)
            relative_y = float(t) * math.cos(scan_angle)

            # the robots location has to match the world_reduction being done.  confusing as hell

            world_x = relative_x + float(robot.location[0])
            world_y = relative_y + float(robot.location[1])

            width, height = self.world_image.size
            if world_x >= width or world_x < 0. or world_y >= height or world_y < 0.:
                return False

            try:
                r, g, b = self.world_image.getpixel((int(world_x), int(world_y)))
            except IndexError:
                print "out of range"

            if r == 0 and g == 0 and b == 0:
                int_x = int(round(relative_x, 0))
                int_y = int(round(relative_y, 0))
                return [int_x, int_y]

        return False


class Robot:
    """
        Represents the Robots position and Orientation
    """

    def __init__(self, location, orientation):
        self.previous_location = [0.0, 0.0]
        self.previous_location[0] = location[0]
        self.previous_location[1] = location[1]
        self.location = [0.0, 0.0]
        self.location[0] = location[0]
        self.location[1] = location[1]
        self.orientation = orientation

    # TODO setting the location is not valid. We don't know the location.  We only know the motions.
    def set_location(self, location):
        self.previous_location[0] = self.location[0]
        self.previous_location[1] = self.location[1]
        self.location[0] = location[0]
        self.location[1] = location[1]

    def set_motion(self, motion):
        self.previous_location[0] = self.location[0]
        self.previous_location[1] = self.location[1]
        self.location[0] = self.location[0] + motion[0]
        self.location[1] = self.location[1] + motion[1]

    def get_motion(self):
        return [self.location[0] - self.previous_location[0], self.location[1] - self.previous_location[1]]

    def set_orientation(self, orientation):
        self.orientation = orientation

    def get_location(self):
        return self.location


class Landmarks:
    size = 0
    coordinates = []

    def add(self, value):
        if value not in self.coordinates:
            if len(self.coordinates) < self.size:
                self.coordinates.append(value)
            else:
                self.coordinates.insert(self.size, value)
            self.size += 1

    def get_coordinates(self):
        return self.coordinates[:self.size]

    def reset(self):
        self.size = 0


import sys


def test():
    scanner = Scanner("floor3.png")

    landmarks = Landmarks()

    print "type: x y angle"

    while True:

        line = sys.stdin.readline()
        scanned_image = Image.new("RGB", scanner.world_image_img.size, "white")
        line = line.replace('\n', '', 1)
        params = line.split(" ")
        x_in = int(params[0])
        y_in = int(params[1])
        orientation_in = float(params[2])
        robot = Robot([x_in, y_in], orientation_in)

        scanner.scan_world(robot, landmarks)

        draw = ImageDraw.Draw(scanned_image)

        draw.rectangle(((0, 0), scanner.world_image_img.size), 'white')

        print "landmarks", landmarks.size
        for i in range(landmarks.size):
            landmark = landmarks.coordinates[i]
            robot_loc = robot.get_location()
            draw.point((landmark[0] + robot_loc[0], landmark[1] + robot_loc[1]), "black")
            # draw.point((landmark[0], landmark[1]), "black")

        draw.rectangle(((x_in, y_in), (x_in + 5, y_in + 5)), "red")

        scanned_image.save("scan.jpeg")

if __name__ == "__main__":
    test()
import mock_scanner as sc
import kalman4d
import time
import scipy.spatial.distance as distance
import math
# from munkres import Munkres
import hungarian
import pygame
import numpy as np

class Graphics:

    def __init__(self):
        self.BLACK = (0, 0, 0)
        self.WHITE = (255, 255, 255)
        self.RED = (255, 0, 0)
        self.GREEN = (0, 255, 0)
        self.BLUE = (0, 0, 255)
        pygame.init()
        self.DISPLAYSURF = pygame.display.set_mode((459, 303), 0, 32)
        pygame.display.set_caption('Drawing')

    def clear(self):
        pygame.draw.rect(self.DISPLAYSURF, self.WHITE, (0, 0, 459, 303))
        pygame.display.update()

    def draw_observations(self, observations):
        for o in observations:
            pygame.draw.rect(self.DISPLAYSURF, self.BLACK, (o.x, o.y, 2, 2))
        pygame.display.update()

    def draw_robot(self, robot):
        pygame.draw.rect(self.DISPLAYSURF, self.GREEN, (robot.get_location().x, robot.get_location().y, 5, 5))

    def draw_kalmans(self, kalmans):
        for i in range(len(kalmans)):
            kalman = kalmans[i]
            pygame.draw.rect(self.DISPLAYSURF, self.RED, (kalman.predicted_x(), kalman.predicted_y(), 2, 2))
        pygame.display.update()

# graphics = Graphics()
# graphics.clear()

scanner = sc.Scanner("floor.jpeg")

robot = sc.Robot([0, 100], 0)
landmarks = sc.Landmarks()
kalmans = []
robot_x_loc = 0;
world_reduction = 5

now = time.time()

for step in range(20):

    # graphics.clear()

    robot_x_loc += 10
    robot.set_location([robot_x_loc, 100])

    # graphics.draw_robot(robot)

    scanner.scan_world(robot, landmarks) # observations go into landmarks
    observations = landmarks.coordinates[:landmarks.size] # get observations
    observations = np.array(observations)

    if world_reduction > 1:
        # reduce observations
        observations = np.divide(observations, 5)
        contiguous = np.ascontiguousarray(observations).view(np.dtype((np.void, observations.dtype.itemsize * observations.shape[1])))
        observations = np.unique(contiguous).view(observations.dtype).reshape(-1, observations.shape[1])

    # graphics.draw_observations(observations)

    if len(kalmans) == 0:
        # assign a kalman filter to each landmark
        for coordinate in observations:
            fit, y = coordinate[0], coordinate[1]
            kalmans.append(kalman4d.Kalman4d([float(fit), float(y)]))

    else:
        estimates = []

        # run all Kalman predictions
        for kalman in kalmans:
            kalman.prediction()
            estimates.append([kalman.predicted_x(), kalman.predicted_y()])

        distances = [[o[0], o[1]] for o in observations] + estimates

        # get Euclidean distance of all observations and estimates
        diffs = distance.pdist(distances)
        diffs = distance.squareform(diffs)

        # hungarian requires a square matrix.  Remove extra observations, or estimates from the hungarian result later.
        square_len = len(observations) if len(observations) >= len(estimates) else len(estimates)
        diffs = [row[-1 * square_len:] for row in diffs[:square_len]] # trim down to distances between observations and estimates

        ###  perfect square
        #        est1  est2 est3
        #  obs1  1.2   3    4
        #  obs2  1     1    .2
        #  obs3  0     0.2  1

        ###  more observations
        #        est1  est2
        #  obs1  1.2   3
        #  obs2  1     1
        #  obs3  0     0.2

        ###  more estimates
        #        est1  est2
        #  obs1  1.2   3
        #  obs2  1     1
        #  obs3  0     0.2

        diffs = np.array(diffs)
        hungarian_fit = np.array(hungarian.lap(diffs)).T # hungarian algorithm for best fits
        best_fits = []

        # hungarian output is (row, column)
        # observations are row, estimates are column

        # remove anything added to square the difference matrix
        if len(observations) > len(estimates):
            print len(observations) - len(estimates), "more observations"
            take = len(observations) - len(estimates)
            for fit in hungarian_fit:
                if fit[1] >= take:
                    best_fits.append((fit[0], fit[1] - take))
                else:
                    observation = observations[fit[0]]
                    kalmans.append(kalman4d.Kalman4d([float(observation[0]), float(observation[1])]))

        elif len(estimates) > len(observations):
            print len(estimates) - len(observations), "more estimates"
            take = len(estimates) - len(observations)
            for fit in hungarian_fit:
                if fit[0] >= take:
                    best_fits.append((fit[0] - take, fit[1]))
                    # extra estimates can be ignored


        else:
            print "equal estimates and observations"
            best_fits = hungarian_fit

        # feed all observations to the kalman filters they're closest to
        tracked_kalmans = []

        for best_fit in best_fits:
            est_idx = best_fit[1]
            observed = observations[best_fit[0]]
            kalman = kalmans[best_fit[1]]
            kalman.measure([observed[0], observed[1]])
            tracked_kalmans.append(kalman)

        kalmans = tracked_kalmans

        # purge any old untracked estimates

        # graphics.draw_kalmans(kalmans)

        # raw_input("Press Enter to continue...")

print "total time", time.time() - now
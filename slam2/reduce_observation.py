import numpy as np


class Reduction:
    def __init__(self, reduction_factor=5):
        self.reduction_factor = reduction_factor

    def reduction(self, xy):
        if len(xy) == 0:
            return xy

        if type(xy) is not np.ndarray:
            raise Exception("reduction must be a numpy array")
        if len(xy.shape) != 2 or xy.shape[1] != 2:
            raise Exception("must be an array of 2 element arrays, shape of (any, 2)")

        mapped = np.apply_along_axis(self._reduce_elements, 1, xy)
        unique_map = _unique(mapped)
        return unique_map

    def _reduce_elements(self, el):
        return [el[0] / self.reduction_factor, el[1] / self.reduction_factor]


def _unique(array):
    array = np.ascontiguousarray(array)
    unique_a = np.unique(array.view([('', array.dtype)] * array.shape[1]))
    return unique_a.view(array.dtype).reshape((unique_a.shape[0], array.shape[1]))

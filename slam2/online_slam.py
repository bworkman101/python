import numpy as np
import random
import scipy.spatial.distance as scipy_dist
import sklearn.utils.linear_assignment_ as hungarian
import kalman4d


####
#   TODO cubify the input.  Reduce the sample space.
#

# ------------
# User Instructions
#
# In this problem you will implement a more manageable
# version of graph SLAM in 2 dimensions.
#
# Define a function, online_slam, that takes 5 inputs:
# data, N, num_landmarks, motion_noise, and
# measurement_noise--just as was done in the last
# programming assignment of unit 6. This function
# must return TWO matrices, mu and the final Omega.
#
# Just as with the quiz, your matrices should have x
# and y interlaced, so if there were two poses and 2
# landmarks, mu would look like:
#
# mu = matrix([[Px0],
#              [Py0],
#              [Px1],
#              [Py1],
#              [Lx0],
#              [Ly0],
#              [Lx1],
#              [Ly1]])
#
# Enter your code at line 566.

# -----------
# Testing
#
# You have two methods for testing your code.
#
# 1) You can make your own data with the make_data
#    function. Then you can run it through the
#    provided slam routine and check to see that your
#    online_slam function gives the same estimated
#    final robot pose and landmark positions.
# 2) You can use the solution_check function at the
#    bottom of this document to check your code
#    for the two provided test cases. The grading
#    will be almost identical to this function, so
#    if you pass both test cases, you should be
#    marked correct on the homework.


# ------------------------------------------------
#
# this is the matrix class
# we use it because it makes it easier to collect constraints in GraphSLAM
# and to calculate solutions (albeit inefficiently)
#

def print_m(matrix):
    """
    this is the matrix class
    we use it because it makes it easier to collect constraints in GraphSLAM
    and to calculate solutions (albeit inefficiently)
    :param matrix:
    :return:
    """
    print " ++++++++ "
    for i in range(len(matrix)):
        row = ""
        for j in range(len(matrix[0])):
            row += str(matrix[i][j])
            if j % 2 == 1:
                row += "|"
            else:
                row += ", "
        print row
        if i % 2 == 1:
            print "----------------------------------------------"

# ######################################################################

# ------------------------------------------------
#
# this is the robot class
#
# our robot lives in x-y space, and its motion is
# pointed in a random direction. It moves on a straight line
# until is comes close to a wall at which point it turns
# away from the wall and continues to move.
#
# For measurements, it simply senses the x- and y-distance
# to landmarks. This is different from range and bearing as
# commonly studies in the literature, but this makes it much
# easier to implement the essentials of SLAM without
# cluttered math
#


# class Robot:
#
#     # --------
#     # init:
#     #   creates robot and initializes location to 0, 0
#     #
#
#     def __init__(self, world_size=100.0, measurement_range=30.0,
#                  motion_noise=1.0, measurement_noise=1.0):
#         self.measurement_noise = 0.0
#         self.world_size = world_size
#         self.measurement_range = measurement_range
#         self.x = world_size / 2.0
#         self.y = world_size / 2.0
#         self.motion_noise = motion_noise
#         self.measurement_noise = measurement_noise
#         self.landmarks = []
#         self.num_landmarks = 0
#
#     @staticmethod
#     def rand():
#         return random.random() * 2.0 - 1.0
#
#     # --------
#     #
#     # make random landmarks located in the world
#     #
#
#     def make_landmarks(self, num_landmarks):
#         self.landmarks = []
#         for i in range(num_landmarks):
#             self.landmarks.append([round(random.random() * self.world_size),
#                                    round(random.random() * self.world_size)])
#         self.num_landmarks = num_landmarks
#
#     # --------
#     #
#     # move: attempts to move robot by dx, dy. If outside world
#     #       boundary, then the move does nothing and instead returns failure
#     #
#
#     def move(self, dx, dy):
#
#         x = self.x + dx + self.rand() * self.motion_noise
#         y = self.y + dy + self.rand() * self.motion_noise
#
#         if x < 0.0 or x > self.world_size or y < 0.0 or y > self.world_size:
#             return False
#         else:
#             self.x = x
#             self.y = y
#             return True
#
#     # --------
#     #
#     # sense: returns x- and y- distances to landmarks within visibility range
#     #        because not all landmarks may be in this range, the list of measurements
#     #        is of variable length. Set measurement_range to -1 if you want all
#     #        landmarks to be visible at all times
#     #
#
#     def sense(self):
#         z = []
#         for i in range(self.num_landmarks):
#             dx = self.landmarks[i][0] - self.x + self.rand() * self.measurement_noise
#             dy = self.landmarks[i][1] - self.y + self.rand() * self.measurement_noise
#             if self.measurement_range < 0.0 or abs(dx) + abs(dy) <= self.measurement_range:
#                 z.append([i, dx, dy])
#         return z
#
#     # --------
#     #
#     # print robot location
#     #
#
#     def __repr__(self):
#         return 'Robot: [x=%.5f y=%.5f]' % (self.x, self.y)

# ------------
# Strange matrix expansion.
# TODO figure out how to replace this
#
# creates a new matrix from the existing matrix elements.
#
# Example:
#       l = matrix([[1, 2, 3],
#                  [4, 5, 6]])
#
#       l.expand(3, 5, [0, 2], [0, 2, 3])
#
# results in:
#
#       [[1, 0, 2, 3, 0],
#        [0, 0, 0, 0, 0],
#        [4, 0, 5, 6, 0]]
#
# expand is used to introduce new rows and columns into an existing matrix
# list1/list2 are the new indexes of row/columns in which the matrix
# elements are being mapped. Elements for rows and columns
# that are not listed in list1/list2
# will be initialized by 0.0.
#


def arr_expand(arr, dimx, dimy, list1, list2=None):
    if None == list2:
        list2 = list1
    if len(list1) > arr.shape[0] or len(list2) > arr.shape[1]:
        raise ValueError, "list invalid in expand()"

    res = np.zeros((dimx, dimy))
    for i in range(len(list1)):
        for j in range(len(list2)):
            res[list1[i]][list2[j]] = arr[i][j]
    return res


# ------------
#
# creates a new matrix from the existing matrix elements.
#
# Example:
#       l = matrix([[ 1,  2,  3,  4,  5],
#                   [ 6,  7,  8,  9, 10],
#                   [11, 12, 13, 14, 15]])
#
#       l.take([0, 2], [0, 2, 3])
#
# results in:
#
#       [[1, 3, 4],
#        [11, 13, 14]]
#
#
# take is used to remove rows and columns from existing matrices
# list1/list2 define a sequence of rows/columns that shall be taken
# is no list2 is provided, then list2 is set to list1 (good for symmetric matrices)
#
def arr_take(arr, list1, list2=None):
    if None == list2:
        list2 = list1
    return np.take(np.take(arr, list1, 0), list2, 1)


class MakeRelative:
    """
    Make the coordinates recorded in xi relative to the robots postions.
    This helps us match observations.
    """
    def __init__(self, robot_loc):
        """
        :param robot_loc: robot location in [x,y]
        :return:
        """
        self.robot_loc = robot_loc

    def do(self, xy):
        relative_x = xy[0] - self.robot_loc[0]
        relative_y = xy[1] - self.robot_loc[1]
        return [relative_x, relative_y]


class ObservationNode:
    """
    Holds land information collected in a landmark observation
    """
    def __init__(self, initial_observation):
        """
        :param landmark_idx: the index in mu of the landmark
        :return:
        """
        self.obsrv_kalman = kalman4d.Kalman4d(initial_xy=initial_observation, mu=0.1, record=True)
        self.obsrv_kalman.prediction()

    def new_observation(self, observation):
        self.obsrv_kalman.measure(observation)
        self.obsrv_kalman.prediction()

    def get_obsrv_kalman(self):
        return self.obsrv_kalman

    def __str__(self):
        return self.obsrv_kalman.__str__()

debug_file = open("debug.txt", "w")

class OnlineSlam:

    def __init__(self):
        self.new_landmark_idx = 0
        self.nodes = {}
        self.mu = None
        self.omega = None
        self.xi = None

    def match_landmarks(self, observations):
        """

        :param mu: the landmarks tracked in x
                             is produced by do_slam()
                             array of [[robot_x], [robot_y], [landmark_1_x], [landmark_1_y], ...]]
        :param observations: [x, y]
        :param landmark nodes
        :return: landmarks matched to landmarks in xi, or new ones
                 [ [ landmark_idx, landmark_x, landmark_y ], [ ... ] ]
                 landmark_idx is landmark index in slam matrix
        """
        if len(observations) == 0:
            return []

        elif self.mu is None or len(self.mu) == 2:
            landmarks = []
            for i in range(len(observations)):
                landmarks.append([self.new_landmark_idx, observations[i][0], observations[i][1]])
                self.new_landmark_idx += 1
            return landmarks

        else:
            mu_landmarks = self._normalize_mu_landmarks()

            best_fits = self._best_fit(mu_landmarks, observations)

            # hungarian output is (row, column)
            # xi is row, obs is column

            landmarks = []

            unfit_idx = [i for i in range(len(observations))]

            debug_file.write("\n\n--> new fit ------------------------------------------------------------->\n")

            for fit in best_fits:

                observation = observations[fit[1]]
                node = self.nodes[fit[0]]
                obsrv_kalman = node.get_obsrv_kalman()
                unfit_idx.remove(fit[1])
                mu_lm = ((mu_landmarks[fit[0]][0] + self.mu[0])[0],
                          (mu_landmarks[fit[0]][1] + self.mu[1])[0])
                mu_rel = (mu_landmarks[fit[0]][0],
                          mu_landmarks[fit[0]][1])
                mu_loc = ((self.mu[0])[0], (self.mu[1])[0])
                predicted = (obsrv_kalman.x[0][0], obsrv_kalman.x[1][0])
                real_observation = ((observation[0] + self.mu[0])[0],
                                    (observation[1] + self.mu[1])[0])
                relative_observation = (observation[0],
                                        observation[1])
                predicts = obsrv_kalman.predicts(observation[0], observation[1], mu_rel[0], mu_rel[1])
                variance = (obsrv_kalman.max_x(), obsrv_kalman.max_y())

                if predicts == False:
                    print "false"

                out = "\n---> predicted {}" \
                      "\n     relative_observation {}" \
                      "\n     predicts {}" \
                      "\n     mu_lm {}" \
                      "\n     mu_rel {}" \
                      "\n     observation {}" \
                      "\n     mu_loc {}" \
                      "\n     real_observation {}" \
                      "\n     variance {}" \
                      "\n     history {}\n".format(
                            predicted,
                            relative_observation,
                            predicts,
                            mu_lm,
                            mu_rel,
                            observation,
                            mu_loc,
                            real_observation,
                            variance,
                            obsrv_kalman.history)

                debug_file.write(out)

                if predicts:
                    landmarks.append([fit[0], observation[0], observation[1]])
                else:
                    landmarks.append([self.new_landmark_idx, observation[0], observation[1]])
                    self.new_landmark_idx += 1

            for unfit in unfit_idx:
                landmarks.append([self.new_landmark_idx, observations[unfit][0], observations[unfit][1]])
                self.new_landmark_idx += 1

            return landmarks

    @staticmethod
    def _best_fit(source, target):
        """
        remove robot pose, and reshape from [[x1],[y2],[x2],[y2]] to [[x1,y1],[x2,y2]]
        :param source:
        :param target:
        :return:
        """
        distances = np.concatenate((source, target), axis=0)
        diffs = scipy_dist.pdist(distances)
        diffs = scipy_dist.squareform(diffs)
        diffs = [row[-1 * len(target):] for row in diffs[:len(source)]]

        # perfect square
        #           tar1  tar2 tar3
        #  source_1  1.2   3    4
        #  source_2  1     1    .2
        #  source_3  0     0.2  1
        hungarian_fit = hungarian.linear_assignment(np.array(diffs))

        return hungarian_fit

    def _normalize_mu_landmarks(self):
        """
        Remake the arrays to fit slam
        Make the slam locations relative to the robot.
        """
        mu_landmarks = self.mu[2:].reshape(((self.mu.shape[0] - 2) / 2, 2))
        robot_location = self.mu[:2].reshape((1, 2))[0]
        make_relative = MakeRelative(robot_location)
        mu_landmarks = np.apply_along_axis(make_relative.do, 1, mu_landmarks)
        return mu_landmarks

    def expand_xi_omega(self, measurement):
        """
        Expands omega and xi to fit the landmarks in measurement
        :param measurement: array of [[land_mark_num, loc_x, loc_y], [land_mark_num, loc_x, loc_y]]
        """
        if len(measurement) > 0:
            original_len = len(self.xi) - 2
            measurement = np.array(measurement)
            max_idx = np.amax(measurement[:, 0]) + 1   # indexes are zero based
            diff = max_idx * 2 - original_len

            if diff > 0:
                self.xi = np.append(self.xi, np.zeros((diff, 1)), 0)
                self.omega = np.append(self.omega, np.zeros((diff, len(self.omega))), 0)
                self.omega = np.append(self.omega, np.zeros((len(self.omega), diff)), 1)

    def do_slam(self, measurement, measurement_noise, motion, motion_noise):
        """
        Perform online_slam algorithm.

        :param measurement: array of [[land_mark_num, loc_x, loc_y], [land_mark_num, loc_x, loc_y]]
        :param measurement_noise:
        :param motion:  array of [x_move, y_move]
        :param motion_noise:
        """
        num_landmarks = (len(self.omega) - 2) / 2
        expand_list = [0, 1] + [ex + 4 for ex in range(num_landmarks * 2)]
        take_list = [ex + 2 for ex in range((num_landmarks + 1) * 2)]

        # integrate the measurements
        for i in range(len(measurement)):

            # m is the index of the landmark coordinate in the matrix/vector
            landmark_index = measurement[i][0]
            m = 2 * (1 + landmark_index)

            if landmark_index in self.nodes:
                node = self.nodes[landmark_index]
                node.new_observation([measurement[i][1], measurement[i][2]])
            else:
                self.nodes[landmark_index] = ObservationNode([measurement[i][1], measurement[i][2]])

            # update the information maxtrix/vector based on the measurement
            for b in range(2):
                self.omega[b][b] += 1.0 / measurement_noise
                self.omega[m + b][m + b] += 1.0 / measurement_noise
                self.omega[b][m + b] += -1.0 / measurement_noise
                self.omega[m + b][b] += -1.0 / measurement_noise
                self.xi[b][0] += -measurement[i][1 + b] / measurement_noise
                self.xi[m + b][0] += measurement[i][1 + b] / measurement_noise

        new_dim = 2 + len(self.omega)
        self.omega = arr_expand(self.omega, new_dim, new_dim, expand_list, expand_list)
        self.xi = arr_expand(self.xi, new_dim, 1, expand_list, [0])

        # update the information maxtrix/vector based on the robot motion
        for b in range(4):
            self.omega[b][b] += 1.0 / motion_noise
        for b in range(2):
            self.omega[b][b + 2] += -1.0 / motion_noise
            self.omega[b + 2][b] += -1.0 / motion_noise
            self.xi[b][0] += -motion[b] / motion_noise
            self.xi[b + 2][0] += motion[b] / motion_noise

        # shrink the matrix
        a = arr_take(self.omega, [0, 1], take_list)
        b = arr_take(self.omega, [0, 1])
        self.omega_p = arr_take(self.omega, take_list)
        xi_p = arr_take(self.xi, take_list, [0])
        c = arr_take(self.xi, [0, 1], [0])
        self.omega = self.omega_p - np.dot(np.dot(a.T, np.linalg.inv(b)), a)
        self.xi = xi_p - np.dot(np.dot(a.transpose(), np.linalg.inv(b)), c)

        # compute best estimate
        self.mu = np.dot(np.linalg.inv(self.omega), self.xi)

    def init_xi_and_omega(self, num_landmarks, robot_x=50., robot_y=50.):

        # Set the dimension of the filter
        dim = 2 * (1 + num_landmarks)

        # make the constraint information matrix and vector
        self.omega = np.zeros((dim, dim))
        self.omega[0][0] = 1.0
        self.omega[1][1] = 1.0

        self.xi = np.zeros((dim, 1))
        self.xi[0][0] = robot_x
        self.xi[1][0] = robot_y

        self.mu = [robot_x, robot_y]

    # ######################################################################
    # --------------------------------
    #
    # online_slam - retains all landmarks but only most recent robot pose
    #
    # data: [
    #           [[land_mark_num, loc_x, loc_y], [motion_x, motion_y]]
    #        ]
    #
    def online_slam(self, data, num_landmarks, motion_noise, measurement_noise, robot_x=50., robot_y=50.):

        self.init_xi_and_omega(num_landmarks, robot_x, robot_y)

        # process the data

        for k in range(len(data)):

            measurement = data[k][0]
            motion = data[k][1]
            self.do_slam(measurement, measurement_noise, motion, motion_noise)
            print "\n", self.xi, "\n", self.omega

        return self.mu, self.omega, self.xi  # make sure you return both of these matrices to be marked correct.


def print_result(N, num_landmarks, result):
    """
    print the result of SLAM, the robot pose(s) and the landmarks
    :param N:
    :param num_landmarks:
    :param result:
    :return:
    """
    print
    print 'Estimated Pose(s):'
    for i in range(N):
        print '    ['+ ', '.join('%.3f'%x for x in result.value[2*i]) + ', ' \
              + ', '.join('%.3f'%x for x in result.value[2*i+1]) +']'
    print
    print 'Estimated Landmarks:'
    for i in range(num_landmarks):
        print '    ['+ ', '.join('%.3f'%x for x in result.value[2*(N+i)]) + ', ' \
              + ', '.join('%.3f'%x for x in result.value[2*(N+i)+1]) +']'

# ------------------------------------------------------------------------
#
# Main routines
#

# measurement_range = 50.0     # range at which we can sense landmarks
# motion_noise = 2.0      # noise in robot motion
# measurement_noise = 2.0      # noise in the measurements
# distance = 20.0     # distance by which robot (intends to) move each iteratation


# Uncomment the following three lines to run the full slam routine.

#data = make_data(N, num_landmarks, world_size, measurement_range, motion_noise, measurement_noise, distance)
#result = slam(data, N, num_landmarks, motion_noise, measurement_noise)
#print_result(N, num_landmarks, result)

# Uncomment the following three lines to run the online_slam routine.

#data = make_data(N, num_landmarks, world_size, measurement_range, motion_noise, measurement_noise, distance)
#result = online_slam(data, N, num_landmarks, motion_noise, measurement_noise)
#print_result(1, num_landmarks, result[0])

##########################################################

# ------------
# TESTING
#
# Uncomment one of the test cases below to check that your
# online_slam function works as expected.

def solution_check(result, answer_mu, answer_omega):

    if len(result) != 2:
        print "Your function must return TWO matrices, mu and Omega"
        return False

    user_mu = result[0]
    user_omega = result[1]

    if user_mu.shape[0] == answer_omega.shape[0] and user_mu.shape[1] == answer_omega.shape[1]:
        print "It looks like you returned your results in the wrong order. Make sure to return mu then Omega."
        return False

    if user_mu.shape[0] != answer_mu.shape[0] or user_mu.shape[1] != answer_mu.shape[1]:
        print "Your mu matrix doesn't have the correct dimensions. Mu should be a", answer_mu.shape[0], " x ", answer_mu.shape[1], "matrix."
        return False
    else:
        print "Mu has correct dimensions."

    if user_omega.shape[0] != answer_omega.shape[0] or user_omega.shape[1] != answer_omega.shape[1]:
        print "Your Omega matrix doesn't have the correct dimensions. Omega should be a", answer_omega.shape[0], " x ", answer_omega.shape[1], "matrix."
        return False
    else:
        print "Omega has correct dimensions."

    if not np.allclose(user_mu, answer_mu):
        print "Mu has incorrect entries. user_mu ", user_mu, " == answer_mu ", answer_mu
        return False
    else:
        print "Mu correct."

    if not np.allclose(user_omega, answer_omega):
        print "Omega has incorrect entries."
        return False
    else:
        print "Omega correct."

    print "Test case passed!"
    return True

# -----------
# Test Case 1

testdata1 = [[[[1, 21.796713239511305, 25.32184135169971], [2, 15.067410969755826, -27.599928007267906]], [16.4522379034509, -11.372065246394495]],
                      [[[1, 6.1286996178786755, 35.70844618389858], [2, -0.7470113490937167, -17.709326161950294]], [16.4522379034509, -11.372065246394495]],
                      [[[0, 16.305692184072235, -11.72765549112342], [2, -17.49244296888888, -5.371360408288514]], [16.4522379034509, -11.372065246394495]],
                      [[[0, -0.6443452578030207, -2.542378369361001], [2, -32.17857547483552, 6.778675958806988]], [-16.66697847355152, 11.054945886894709]]]

answer_mu1 = np.array([[81.63549976607898],
                         [27.175270706192254],
                         [98.09737507003692],
                         [14.556272940621195],
                         [71.97926631050574],
                         [75.07644206765099],
                         [65.30397603859097],
                         [22.150809430682695]])

answer_omega1 = np.array([[0.36603773584905663, 0.0,        -0.169811320754717, 0.0,    -0.011320754716981133, 0.0,     -0.1811320754716981, 0.0],
                             [0.0, 0.36603773584905663,         0.0, -0.169811320754717,    0.0, -0.011320754716981133,     0.0, -0.1811320754716981],
                             [-0.169811320754717, 0.0,          0.6509433962264151, 0.0,   -0.05660377358490567, 0.0,      -0.40566037735849064, 0.0],
                             [0.0, -0.169811320754717,          0.0, 0.6509433962264151,    0.0, -0.05660377358490567,      0.0, -0.40566037735849064],
                             [-0.011320754716981133, 0.0,      -0.05660377358490567, 0.0,   0.6962264150943396, 0.0,       -0.360377358490566, 0.0],
                             [0.0, -0.011320754716981133,      0.0, -0.05660377358490567,   0.0, 0.6962264150943396,        0.0, -0.360377358490566],
                             [-0.1811320754716981, 0.0,        -0.4056603773584906, 0.0,   -0.360377358490566, 0.0,         1.2339622641509433, 0.0],
                             [0.0, -0.1811320754716981,        0.0, -0.4056603773584906,    0.0, -0.360377358490566,        0.0, 1.2339622641509433]])

# mu, omega, xi = online_slam(testdata1, 3, 2.0, 2.0, 50., 50.)
# solution_check((mu, omega), answer_mu1, answer_omega1)


# -----------
# Test Case 2

testdata2 = [[[[0, 12.637647070797396, 17.45189715769647], [1, 10.432982633935133, -25.49437383412288]], [17.232472057089492, 10.150955955063045]],
              [[[0, -4.104607680013634, 11.41471295488775], [1, -2.6421937245699176, -30.500310738397154]], [17.232472057089492, 10.150955955063045]],
              [[[0, -27.157759429499166, -1.9907376178358271], [1, -23.19841267128686, -43.2248146183254]], [-17.10510363812527, 10.364141523975523]],
              [[[0, -2.7880265859173763, -16.41914969572965], [1, -3.6771540967943794, -54.29943770172535]], [-17.10510363812527, 10.364141523975523]],
              [[[0, 10.844236516370763, -27.19190207903398], [1, 14.728670653019343, -63.53743222490458]], [14.192077112147086, -14.09201714598981]]]

answer_mu2 = np.array([[63.37479912250136],
                       [78.17644539069596],
                       [61.33207502170053],
                       [67.10699675357239],
                       [62.57455560221361],
                       [27.042758786080363]])

answer_omega2 = np.array([[0.22871751620895048, 0.0, -0.11351536555795691, 0.0, -0.11351536555795691, 0.0],
                          [0.0, 0.22871751620895048, 0.0, -0.11351536555795691, 0.0, -0.11351536555795691],
                          [-0.11351536555795691, 0.0, 0.7867205207948973, 0.0, -0.46327947920510265, 0.0],
                          [0.0, -0.11351536555795691, 0.0, 0.7867205207948973, 0.0, -0.46327947920510265],
                          [-0.11351536555795691, 0.0, -0.46327947920510265, 0.0, 0.7867205207948973, 0.0],
                          [0.0, -0.11351536555795691, 0.0, -0.46327947920510265, 0.0, 0.7867205207948973]])

# now = time.time()
# online_slam = OnlineSlam()
# mu, omega, xi = online_slam.online_slam(testdata2, 2, 3.0, 4.0, 50.0, 50.0)
# solution_check((mu, omega), answer_mu2, answer_omega2)
# print "total time", time.time() - now

testdata3 = [[[[0,  5.,  5.], [1,  5.,  10.], [2,  -3., -5.]], [5., 0.]],
             [[[0,  0.,  5.], [1,  0.,  10.], [2,  -8., -5.]], [5., 0.]],
             [[[0, -5.,  5.], [1, -5.,  10.], [2, -13., -5.]], [5., 0.]],
             [[[0, -10., 5.], [1, -10., 10.], [2, -18., -5.]], [5., 0.]]]
             # [[[0, -15., 5.], [1, -15., 10.], [2, -23., -5.]], [5., 0.]],
             # [[[0, -20., 5.], [1, -20., 10.], [2, -28., -5.]], [5., 0.]],
             # [[[0, -25., 5.], [1, -25., 10.], [2, -33., -5.]], [5., 0.]],
             # [[[0, -30., 5.], [1, -30., 10.], [2, -38., -5.]], [5., 0.]],
             # [[[0, -35., 5.], [1, -35., 10.], [2, -43., -5.]], [5., 0.]],
             # [[[0, -40., 5.], [1, -40., 10.], [2, -48., -5.]], [5., 0.]],
             # [[[0, -45., 5.], [1, -45., 10.], [2, -53., -5.]], [5., 0.]],
             # [[[0, -50., 5.], [1, -50., 10.], [2, -58., -5.]], [5., 0.]],
             # [[[0, -55., 5.], [1, -55., 10.], [2, -63., -5.]], [5., 0.]],
             # [[[0, -60., 5.], [1, -60., 10.], [2, -68., -5.]], [5., 0.]]]
# online_slam = OnlineSlam()
# mu, omega, xi = online_slam.online_slam(testdata3, 3, 5.0, 5.0, 50., 50.)
# print "------------------------------------------------------------------------------------------------------------------------"
# print_m(mu)
# robot_loc = mu[:2]
# locs = mu[2:]
# for loc_i in range(len(locs) / 2):
#     print "landmark ", loc_i, " [", locs[loc_i * 2] - robot_loc[0], ", ", locs[loc_i * 2 + 1] - robot_loc[1], "]"
# print "------------------------------------------------------------------------------------------------------------------------"
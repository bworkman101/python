import pygame

class Graphics:

    def __init__(self, scale=2, width=320, height=200, disabled=False):
        self.scale = scale
        self.width = width * scale
        self.height = height * scale
        self.disabled = disabled
        self.BLACK = (0, 0, 0)
        self.WHITE = (255, 255, 255)
        self.RED = (255, 0, 0)
        self.GREEN = (0, 255, 0)
        self.BLUE = (0, 0, 255)
        if not disabled:
            pygame.init()
            self.DISPLAYSURF = pygame.display.set_mode((self.width, self.height), 0, 32)
            pygame.display.set_caption('Drawing')

    def clear(self):
        if not self.disabled:
            pygame.draw.rect(self.DISPLAYSURF, self.WHITE, (0, 0, self.width, self.height))
            pygame.display.update()

    def draw_observations(self, observations, robot):
        if not self.disabled:
            for o in observations:
                o_x = o[0] + (robot.get_location()[0])
                o_y = o[1] + (robot.get_location()[1])
                pygame.draw.rect(self.DISPLAYSURF, self.BLACK, (o_x * self.scale, o_y * self.scale, self.scale, self.scale))
            pygame.display.update()

    def draw_robot(self, robot):
        if not self.disabled:
            pygame.draw.rect(self.DISPLAYSURF, self.GREEN, (robot.get_location()[0] * self.scale, robot.get_location()[1] * self.scale, 4 * self.scale, 4 * self.scale))

    def draw_kalmans(self, kalmans, robot, color = (255, 0, 0)):
        if not self.disabled:
            for i in range(len(kalmans)):
                kalman = kalmans[i][0]
                k_x = kalman.predicted_x() + (robot.get_location()[0])
                k_y = kalman.predicted_y() + (robot.get_location()[1])
                pygame.draw.rect(self.DISPLAYSURF, color, (k_x * self.scale, k_y * self.scale, 2 * self.scale, 2 * self.scale))
            pygame.display.update()

    def draw_connection(self, observed, kalman, robot):
        if not self.disabled:
            o_x = observed[0] + (robot.get_location()[0])
            o_y = observed[1] + (robot.get_location()[1])
            k_x = kalman.predicted_x() + (robot.get_location()[0])
            k_y = kalman.predicted_y() + (robot.get_location()[1])
            pygame.draw.line(self.DISPLAYSURF, self.BLUE, (o_x * self.scale, o_y * self.scale),
                             (k_x * self.scale, k_y * self.scale), 1)
            pygame.display.update()

    def draw_grid(self):
        if not self.disabled:
            step = self.scale * 5;
            for w in range(0, self.width, step):
                pygame.draw.line(self.DISPLAYSURF, self.BLACK, (w, 0), (w, self.height), 1)
            for h in range(0, self.height, step):
                pygame.draw.line(self.DISPLAYSURF, self.BLACK, (0, h), (self.width, h), 1)

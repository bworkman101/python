import math

import mock_scanner
import online_slam
from reduce_observation import Reduction
from canvas import Canvas
import numpy as np

# Initialize world and robot
# graphics = Graphics(2, 320, 200)
# graphics = Graphics(disabled=True)
# graphics.clear()
reduction = Reduction(2)
start_loc = reduction.reduction(np.array([[50, 120]]))[0]
# start_loc = np.array([[50, 120]])[0]
robot = mock_scanner.Robot(start_loc, 0)
scanner = mock_scanner.Scanner("floor3.png")
landmarks = mock_scanner.Landmarks()

slam = online_slam.OnlineSlam()

slam.init_xi_and_omega(0, start_loc[0], start_loc[1])

# canv = Canvas(10, 64, 40)
canv = Canvas()

def draw_scan():
    # x, y = reduction.reduction(np.array([robot.get_location()]))[0]
    x, y = robot.get_location()
    r = robot.orientation
    max_r = r + math.pi * (2./5.)
    min_r = r - math.pi * (2./5.)
    canv.draw_line((x, y), max_r, 10)
    canv.draw_line((x, y), min_r, 10)


def run():
    scanner.scan_world(robot, landmarks)
    observations = reduction.reduction(np.array(landmarks.get_coordinates()))
    # observations = np.array(landmarks.get_coordinates())
    matched_lndmrks = slam.match_landmarks(observations)
    slam.expand_xi_omega(matched_lndmrks)
    slam.do_slam(matched_lndmrks, 8.0, robot.get_motion(), 8.0)
    canv.clear()
    canv.commit()
    locs = slam.mu[2:].reshape(((slam.mu.shape[0] - 2) / 2, 2))
    canv.draw_points(locs)
    slam_loc = slam.mu[:2].reshape(1, 2)
    canv.draw_points(slam_loc)
    canv.draw_points([robot.get_location()], 'green')
    draw_scan()
    canv.commit()
    print "mu length {}, s_loc={}, r_loc={}, r={}".format((len(slam.mu) - 2) / 2, slam_loc[0], robot.get_location(), robot.orientation)


if __name__ == "__main__":

    step = 5

    def left():
        robot.set_motion([0, 0])
        rot_steps = 20

        for i in range(rot_steps):
            angle = robot.orientation - (1./2.) / float(rot_steps) * math.pi
            robot.set_orientation(angle)
            run()

    robot.set_orientation(0)

    for i in range(42):
        robot.set_motion([step, 0])
        run()

    left()

    for i in range(20):
        robot.set_motion([0, -step])
        run()

    left()

    for i in range(25):
        robot.set_motion([-step, 0])
        run()

    left()

    for i in range(16):
        robot.set_motion([0, step])
        run()

    left()

    for i in range(20):
        robot.set_motion([step, 0])
        run()

    left()

    for i in range(16):
        robot.set_motion([0, -step])
        run()

    # def left():
    #     robot.set_motion([0, 0])
    #     rot_steps = 20
    #
    #     for i in range(rot_steps):
    #         angle = robot.orientation - (1./2.) / float(rot_steps) * math.pi
    #         robot.set_orientation(angle)
    #         run()
    #
    # robot.set_orientation(0)
    #
    # for i in range(42):
    #     robot.set_motion([5, 0])
    #     run()
    #
    # left()
    #
    # for i in range(20):
    #     robot.set_motion([0, -5])
    #     run()
    #
    # left()
    #
    # for i in range(25):
    #     robot.set_motion([-5, 0])
    #     run()
    #
    # left()
    #
    # for i in range(16):
    #     robot.set_motion([0, 5])
    #     run()
    #
    # left()
    #
    # for i in range(20):
    #     robot.set_motion([5, 0])
    #     run()
    #
    # left()
    #
    # for i in range(16):
    #     robot.set_motion([0, -5])
    #     run()
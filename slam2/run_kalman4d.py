import kalman4d as kalman4d

measurements = [[5., 10.], [6., 8.], [7., 6.], [8., 4.], [9., 2.], [10., 0.]]
xy = [4., 12.]

# measurements = [[1., 4.], [6., 0.], [11., -4.], [16., -8.]]
# xy = [-4., 8.]

# measurements = [[1., 17.], [1., 15.], [1., 13.], [1., 11.]]
# xy = [1., 19.]

#measurements = [[1., 2.], [1., 4.], [6., 13.], [9., 11.], [11., 11.]]
# xy = [1., 1.]

# measurements = [[1., 1.], [1., 2.], [1., 1.], [1., 1.], [1., 1.]]
# xy = [1., 1.]

kalman4d = kalman4d.Kalman4d(xy, .5)

import time
start = time.time()

for measurement in measurements:

    kalman4d.prediction()
    print "after prediction --> measured x=%f y=%-10f" % (measurement[0], measurement[1]), \
        "predicted motion x=%f y=%-10f" % (kalman4d.predicted_motion()[0], kalman4d.predicted_motion()[1]), \
        "variance x=%f y=%f" % (kalman4d.max_x(), kalman4d.max_y())

    kalman4d.prediction()
    print "after prediction --> measured x=%f y=%-10f" % (measurement[0], measurement[1]), \
        "predicted motion x=%f y=%-10f" % (kalman4d.predicted_motion()[0], kalman4d.predicted_motion()[1]), \
        "variance x=%f y=%f" % (kalman4d.max_x(), kalman4d.max_y())

    kalman4d.measure(measurement)
    print "after measure -->    measured x=%f y=%-10f" % (measurement[0], measurement[1]), \
        "predicted motion x=%f y=%-10f" % (kalman4d.predicted_motion()[0], kalman4d.predicted_motion()[1]), \
        "variance x=%f y=%f" % (kalman4d.max_x(), kalman4d.max_y())

print time.time() - start
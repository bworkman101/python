# Kalman filter from Udacity Robot Motion class
# Matrices defined here https://www.udacity.com/wiki/cs373/kalman-filter-matrices

from matrix import matrix

def point_in_ellipse(test, center, width, height):
    dx = test.x - center.x
    dy = test.y - center.y
    return (dx * dx) / (width * width) + (dy * dy) / (height * height) <= 1

class Kalman4d:

    dt = 1

    def __init__(self, initial_xy):
        self.u = matrix([[0.], [0.], [0.], [0.]])
        self.x = matrix([[initial_xy[0]], [initial_xy[1]], [0.], [0.]])  # initial state (location and velocity)
        self.p = matrix([[0., 0., 0., 0.], [0., 0., 0., 0.], [0., 0., 1000., 0.], [0., 0., 0., 1000.]])  # initial uncertainty: 0 for positions x and y, 1000 for the two velocities
        self.f = matrix([[1., 0., self.dt, 0.], [0., 1., 0., self.dt], [0., 0., 1., 0.], [0., 0., 0., 1.]])  # next state function: generalize the 2d version to 4d
        self.h = matrix([[1., 0., 0., 0.], [0., 1., 0., 0.]])  # measurement function: reflect the fact that we observe x and y but not the two velocities
        self.r = matrix([[0.1, 0], [0, 0.1]])  # measurement uncertainty: use 2x2 matrix with 0.1 as main diagonal
        self.i = matrix([[1., 0., 0., 0.], [0., 1., 0., 0.], [0., 0., 1., 0.], [0., 0., 0., 1.]])  # 4d identity matrix

    def predicts(self, x, y):
        """
        Does the prediction step.  Then determines if (x,y) are a prediction
        :param x:
        :param y:
        :return: bool: if the coordinates are predicted outcomes
        """
        # do the values fall within the predicted range
        return self._in_variance(x, self.x[0], self.p[0][0]) and self._in_variance(y, self.x[1], self.p[1][1])

    def predicted_x(self):
        return self.x[0]

    def predicted_y(self):
        return self.x[1]

    def prediction(self):
        # prediction
        self.x = (self.f * self.x) + self.u
        self.p = self.f * self.p * self.f.transpose()
        # print "prediction"
        # self.x.show()
        # self.p.show()
        return self.p, self.x

    def measure(self, measurement):
        # measurement update
        z = matrix([measurement])
        y = z.transpose() - (self.h * self.x)
        s = self.h * self.p * self.h.transpose() + self.r
        k = self.p * self.h.transpose() * s.inverse()
        self.x = self.x + (k * y)
        self.p = (self.i - (k * self.h)) * self.p
        # print "measurement ", measurement
        # self.x.show()
        # self.p.show()
        return self.p, self.x

    def filter(self, measurement):
        self.p, self.x = self.prediction()
        self.p, self.x = self.measure(measurement)


import Image
import ImageDraw
import math


class Canvas:
    def __init__(self, scale=2, width=320, height=200):
        self.BLACK = (0, 0, 0)
        self.WHITE = (255, 255, 255)
        self.scale = scale
        self.width = width * scale
        self.height = height * scale
        self.scanned_image = Image.new("RGB", (self.width, self.height), "white")
        self.clear()

    def clear(self):
        draw = ImageDraw.Draw(self.scanned_image)
        draw.rectangle(((0, 0), (self.width, self.height)), 'white')

    def draw_points(self, points, color='black'):
        """
        :param points: [[x1, y1], [x2, y2]]
        :param color: default black
        :return:
        """
        draw = ImageDraw.Draw(self.scanned_image)

        for point in points:
            x = point[0] * self.scale
            y = point[1] * self.scale
            draw.rectangle(((x, y), (x + self.scale, y + self.scale)), color)

    def draw_line(self, p, angle, len, color='red', width=1):
        draw = ImageDraw.Draw(self.scanned_image)
        x = p[0] * self.scale
        y = p[1] * self.scale
        x2 = len * math.cos(angle) + (p[0]* self.scale)
        y2 = len * math.sin(angle) + (p[1] * self.scale)
        draw.line(((x, y), (x2, y2)), color, width)

    def commit(self):
        self.scanned_image.save("graph.jpeg")

import networkx.algorithms.isomorphism as iso
import networkx as netx

g1 = netx.DiGraph()
g2 = netx.DiGraph()
g1.add_path([1, 2, 3, 4], weight=1)
g2.add_path([10, 20, 30], weight=1)

gm = iso.GraphMatcher(g1, g2)

print gm.subgraph_is_isomorphic()
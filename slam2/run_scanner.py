import mock_scanner as sc
import kalman4d as kalman4d
import scipy.spatial.distance as distance
import sklearn.utils.linear_assignment_ as hungarian
import numpy as np
import time
from graphics import Graphics

class Observer:

    def __init__(self, graphics, robot, kalman_variance=0.1):
        self.kalmans = None
        self.observations = None
        self.graphics = graphics
        self.robot = robot
        self.kalman_variance = kalman_variance

    def observe(self, landmarks, scanner):

        self.graphics.clear()
        self.graphics.draw_grid()
        self.graphics.draw_robot(self.robot)

        scanner.scan_world(self.robot, landmarks)  # observations go into landmarks
        self.observations = landmarks.coordinates[:landmarks.size] # get observations
        self.observations = np.array(self.observations)

        print "reduced observations"
        for o in self.observations:
            print "  x=%f y=%f" % (o[0], o[1])

        self.graphics.draw_observations(self.observations, self.robot)

        if self.kalmans == None or len(self.kalmans) == 0:
            # assign a kalman filter to each landmark
            if len(self.observations) > 0:
                self.kalmans = np.apply_along_axis(self._create_kalman, 1, self.observations)

        else:
            estimates = np.apply_along_axis(self._predict_kalman, 1, self.kalmans)

            hungarian_fit = []

            if len(self.observations) > 0:
                distances = np.concatenate((self.observations, estimates), axis=0)

                print "estimates"
                for r in estimates:
                    print "  x=%f y=%f" % (r[0], r[1])

                print "observations"
                for r in self.observations:
                    print "  x=%f y=%f" % (r[0], r[1])

                # get Euclidean distance of all observations and estimates
                diffs = distance.pdist(distances)
                diffs = distance.squareform(diffs)

                # trim down to distances between observations and estimates
                diffs = [row[-1 * len(estimates):] for row in diffs[:len(self.observations)]]

                ###  perfect square
                #        est1  est2 est3
                #  obs1  1.2   3    4
                #  obs2  1     1    .2
                #  obs3  0     0.2  1

                hungarian_fit = hungarian.linear_assignment(np.array(diffs))

            # hungarian output is (row, column)
            # observations are row, estimates are column
            # remove anything added to square the difference matrix

            # feed all observations to the kalman filters they're closest to

            print "hungarian"
            for r in hungarian_fit:
                print "  e=%d o=%d" % (r[1], r[0])

            self.kalmans = np.array([]) if len(hungarian_fit) == 0 else np.apply_along_axis(self._measure_observed, 1, hungarian_fit)

            if len(self.observations) > len(estimates):
                add_kalmans = self._add_observations(estimates, hungarian_fit)
                self.kalmans = np.vstack((self.kalmans, add_kalmans))

            # purge any old untracked estimates

            self.graphics.draw_kalmans(self.kalmans, self.robot)

            # raw_input("Press Enter to continue...")

    def _measure_observed(self, best_fit):
        observed = self.observations[best_fit[0]]
        kalman = self.kalmans[best_fit[1]]
        if kalman[0].predicts(observed[0], observed[1], self.robot.get_motion()):
            kalman[0].measure([observed[0], observed[1]])
            self.graphics.draw_connection(observed, kalman[0], self.robot)
            return kalman
        else:
            new_kalman = kalman4d.Kalman4d([float(observed[0]), float(observed[1])], self.kalman_variance)
            return [new_kalman]

    def _create_kalman(self, coordinate):
        return [kalman4d.Kalman4d([float(coordinate[0]), float(coordinate[1])], self.kalman_variance)]

    def _predict_kalman(self, kalman):
        kalman[0].prediction()
        motion = self.robot.get_motion()
        return [kalman[0].predicted_x() - motion[0], kalman[0].predicted_y() - motion[1]]

    def _add_observations(self, estimates, hungarian_fit):
        ###  more observations
        #        est1  est2
        #  obs1  1.2   3
        #  obs2  1     1
        #  obs3  0     0.2
        print len(self.observations) - len(estimates), "more observations"
        add_kalmans = []
        sorted_fits = np.sort(hungarian_fit[:, 1])
        sorted_fits_itr = iter(sorted_fits)
        for i in range(len(self.observations)):
            try:
                sorted_next = sorted_fits_itr.next()
            except StopIteration:
                sorted_next = -1

            if i != sorted_next:
                observation = self.observations[i]
                new_kalman = kalman4d.Kalman4d([float(observation[0]), float(observation[1])], self.kalman_variance)
                new_kalman.prediction()
                add_kalmans.append([new_kalman])
        self.graphics.draw_kalmans(add_kalmans, self.robot, self.graphics.GREEN)
        return add_kalmans

def test_run():
    now = time.time()

    world_reduction = 5
    graphics = Graphics(world_reduction, 2, 320, 200)
    # graphics = Graphics(disabled=True)
    graphics.clear()
    robot = sc.Robot([0, 100], 0)
    observer = Observer(graphics, robot)
    scanner = sc.Scanner("floor1.jpeg")
    landmarks = sc.Landmarks()

    # mvs = [[100, 100, 0],
    #        [100, 100, math.pi / 10],
    #        [100, 100, math.pi / 10 * 2],
    #        [100, 100, math.pi / 10 * 3],
    #        [100, 100, math.pi / 10 * 4],
    #        [100, 100, math.pi / 10 * 5],
    #        [100, 100, math.pi / 10 * 6],
    #        [100, 100, math.pi / 10 * 7],
    #        [100, 100, math.pi / 10 * 8],
    #        [100, 100, math.pi / 10 * 9],]

    for i in range(20):
        scan_time = time.time()

        robot.set_motion([5, 0])
        robot.set_orientation(0)

        observer.observe(landmarks, scanner, world_reduction)

        print "total scan iteration time", time.time() - scan_time

    print "total time", time.time() - now

# test_run()
# Kalman filter from Udacity Robot Motion class
# Matrices defined here https://www.udacity.com/wiki/cs373/kalman-filter-matrices
# from matrix import matrix
import numpy as np
import math


class Kalman4d:
    def __init__(self, initial_xy, mu=0.1, record=False, dt=1, vu=10, variance_width=3):
        self.dt = dt
        self.vu = vu
        self.variance_width = variance_width

        self.xy = initial_xy
        self.u = np.array([[0.], [0.], [0.], [0.]])
        self.x = np.array([[initial_xy[0]], [initial_xy[1]], [0.], [0.]])  # initial state (location and velocity)
        self.p = np.array([[0., 0., 0., 0.], [0., 0., 0., 0.], [0., 0., self.vu, 0.], [0., 0., 0., self.vu]])  # initial uncertainty: 0 for positions x and y, 1000 for the two velocities
        self.f = np.array([[1., 0., self.dt, 0.], [0., 1., 0., self.dt], [0., 0., 1., 0.], [0., 0., 0., 1.]])  # next state function: generalize the 2d version to 4d
        self.h = np.array([[1., 0., 0., 0.], [0., 1., 0., 0.]])  # measurement function: reflect the fact that we observe x and y but not the two velocities
        self.r = np.array([[mu, 0], [0, mu]])  # measurement uncertainty: use 2x2 matrix with 0.1 as main diagonal
        self.i = np.array([[1., 0., 0., 0.], [0., 1., 0., 0.], [0., 0., 1., 0.], [0., 0., 0., 1.]])  # 4d identity matrix
        self.history = []
        self.record = record
        self.record_history()

    def predicts(self, x, y, target_x=None, target_y=None):
        """
        determines if (x,y) are a prediction
        :param x:
        :param y:
        :param target_x:
        :param target_y:
        :return: bool: if the coordinates are predicted outcomes
        """
        if target_x == None:
            target_x = self.x[0]

        if target_y == None:
            target_y = self.x[1]

        # do the values fall within the predicted range
        return self.in_variance(x, target_x, self.max_x()) and self.in_variance(y, target_y, self.max_y())

    def max_x(self):
        return self.p[0][0] * self.variance_width + 5

    def max_y(self):
        return self.p[0][2] * self.variance_width + 5

    def prediction(self):
        # prediction
        self.x = np.dot(self.f, self.x) + self.u
        self.p = np.dot(np.dot(self.f, self.p), self.f.transpose())
        # print "prediction"
        # self.x.show()
        # self.p.show()
        return self.p, self.x

    def measure(self, measurement):
        self.xy = measurement
        # measurement update
        z = np.array([measurement])
        y = z.T - np.dot(self.h, self.x)
        hT = self.h.T
        s = np.dot(np.dot(self.h, self.p), hT) + self.r
        s_inv = np.linalg.inv(s)
        k = np.dot(np.dot(self.p, hT), s_inv)
        self.x = self.x + np.dot(k, y)
        self.p = np.dot((self.i - np.dot(k, self.h)), self.p)
        # print "measurement ", measurement
        # self.x.show()
        # self.p.show()

        self.record_history()

        return self.p, self.x

    def record_history(self):
        if self.record:
            self.history.append(self.xy)

    def filter(self, measurement):
        self.p, self.x = self.prediction()
        self.p, self.x = self.measure(measurement)

    def __str__(self):
        return "x=%f y=%f" % (self.x[0], self.x[1])

    def in_variance(self, value, predicted, variance):

        if predicted - variance <= value <= predicted + variance:
            return True
        else:
            return False
            # val_floor, val_ceil = self.bounds(value)
            # pre_floor, pre_ceil = self.bounds(predicted)
            # val = value
            # pre = predicted
            # var = variance
            #
            # if pre_floor - var <= val_ceil and val_floor <= pre_ceil + var:
            #     return True
            # else:
            #     return False

    def bounds(self, predicted):
        if self.round_prediction:
            floor = math.floor(predicted)
            ceil = math.ceil(predicted)
            return floor, ceil
        else:
            return predicted, predicted

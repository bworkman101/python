import io
import picamera
import logging
import socketserver
from threading import Condition
from http import server

PAGE="""\
<html>
<head>
<title>Car Cam</title>
</head>
<body>
<style>
    .console {
        display: flex;
        flex-direction: row;
    }
    .buttons.left {
    }
    .buttons {
        display: flex;
        flex-direction: column;
    }
    .buttons.right {
    }
    .button {
        height: 240px;
        width: 240px;
        font-size: 140px;
    }
    .button.up {
        color: green;
    }
    .button.down {
        color: blue;
    }
    .cam {
        display: inline;
    }
</style>
<script>
    function move(side, dir) {
        let xhttp = new XMLHttpRequest();
        //xhttp.onreadystatechange = function() {
        //    if (this.readyState == 4 && this.status == 200) {
        //        console.log("done")
        //    }
        //};
        xhttp.open("GET", `/move/${side}/${dir}`, true);
        xhttp.send();
    }
</script>
<div class="console">
    <div class="buttons left">
        <input class="button up" type="button" value="&uArr;" onclick="move('l', 'u')"/>
        <input class="button down" type="button" value="&dArr;" onclick="move('l', 'd')"/>
    </div>
    <div class="cam"><img src="stream.mjpg" width="640" height="480"></div>
    <div class="buttons right">
        <input class="button up" type="button" value="&uArr;" onclick="move('r', 'u')"/>
        <input class="button down" type="button" value="&dArr;" onclick="move('r', 'd')"/>
    </div>
</div>
</body>
</html>
"""

class StreamingOutput(object):
    def __init__(self):
        self.frame = None
        self.buffer = io.BytesIO()
        self.condition = Condition()

    def write(self, buf):
        if buf.startswith(b'\xff\xd8'):
            # New frame, copy the existing buffer's content and notify all
            # clients it's available
            self.buffer.truncate()
            with self.condition:
                self.frame = self.buffer.getvalue()
                self.condition.notify_all()
            self.buffer.seek(0)
        return self.buffer.write(buf)

class StreamingHandler(server.BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/':
            self.send_response(301)
            self.send_header('Location', '/index.html')
            self.end_headers()
        elif self.path == '/move/l/u':
            print("left up")
            self.send_response(200)
        elif self.path == '/move/l/d':
            print("left down")
            self.send_response(200)
        elif self.path == '/move/r/u':
            print("right up")
            self.send_response(200)
        elif self.path == '/move/r/d':
            print("right down")
            self.send_response(200)
        elif self.path == '/index.html':
            content = PAGE.encode('utf-8')
            self.send_response(200)
            self.send_header('Content-Type', 'text/html')
            self.send_header('Content-Length', len(content))
            self.send_header('Cache-Control', 'no-cache')
            self.end_headers()
            self.wfile.write(content)
        elif self.path == '/stream.mjpg':
            self.send_response(200)
            self.send_header('Age', 0)
            self.send_header('Cache-Control', 'no-cache, private')
            self.send_header('Pragma', 'no-cache')
            self.send_header('Content-Type', 'multipart/x-mixed-replace; boundary=FRAME')
            self.end_headers()
            try:
                while True:
                    with output.condition:
                        output.condition.wait()
                        frame = output.frame
                    self.wfile.write(b'--FRAME\r\n')
                    self.send_header('Content-Type', 'image/jpeg')
                    self.send_header('Content-Length', len(frame))
                    self.end_headers()
                    self.wfile.write(frame)
                    self.wfile.write(b'\r\n')
            except Exception as e:
                logging.warning(
                    'Removed streaming client %s: %s',
                    self.client_address, str(e))
        else:
            self.send_error(404)
            self.end_headers()

class StreamingServer(socketserver.ThreadingMixIn, server.HTTPServer):
    allow_reuse_address = True
    daemon_threads = True

with picamera.PiCamera(resolution='640x480', framerate=24) as camera:
    output = StreamingOutput()
    #Uncomment the next line to change your Pi's Camera rotation (in degrees)
    #camera.rotation = 90
    camera.start_recording(output, format='mjpeg')
    try:
        address = ('', 8000)
        server = StreamingServer(address, StreamingHandler)
        server.serve_forever()
    finally:
        camera.stop_recording()
To list all thermostats
GET  /thermostats                               Returns a thermostats json object using the id as the key
GET  /thermostat/{id}/{propertyName}            Returns a thermostat property given the thermostats id and propertyName
POST /thermostat/{id}?{propertyName=value}.*    Sets the thermostat property to the given value for the thermostat of the given id. Many propertyName and value pairs can ben given

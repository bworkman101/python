class Thermostat:
	def __init__(self, name, currentTemp, operatingMode, coolSetPoint, heatSetPoint, fanMode):
		self.name = name
		self.currentTemp = currentTemp
		self.operatingMode = operatingMode
		self.coolSetPoint = coolSetPoint
		self.heatSetPoint = heatSetPoint
		self.fanMode = fanMode

        def getName(self):
                return self.name

	def to_JSON(self):
                return {'name': self.name,
                        'currentTemp': self.currentTemp,
                        'operatingMode': self.operatingMode,
                        'coolSetPoint': self.coolSetPoint,
                        'heatSetPoint': self.heatSetPoint,
                        'fanMode': self.fanMode}

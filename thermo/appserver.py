import web
from model import Thermostat
import json

web.config.debug = False

thermostats = {'100': Thermostat('Upstairs Thermostat', 72, 'cool', 72, 71, 'auto'),
               '101': Thermostat('Downstairs Thermostat', 75, 'off', 72, 71, 'off')}

urls = ("/thermostats", "List",
        "/thermostat/(.+)/(.+)", "Access",
        "/thermostat/(.+)", "Access")

app = web.application(urls, globals())

class List:
        
	def GET(self):
                web.header('Content-Type', 'application/json')
		return json.dumps(map(lambda (k,v): (k,v.to_JSON()), thermostats.items()))

class Access:
        
        def GET(self, thermostatId, field):
                web.header('Content-Type', 'application/json')
                
                if thermostatId not in thermostats:
                        raise web.notfound("invalid thermostat id")
                if field not in 'name,currentTemp,operatingMode,coolSetPoint,heatSetPoint,fanMode':
                        raise web.notfound("invalid thermostat property " + field)
                else:
                        return json.dumps(getattr(thermostats[thermostatId], field))

        def PUT(self, thermostatId):
                web.header('Content-Type', 'application/json')

                if thermostatId not in thermostats:
                        raise web.notfound("invalid thermostat id")

                userData = web.input(name=None, operatingMode=None, coolSetPoint=None, heatSetPoint=None, fanMode=None)
                
                if userData.name:
                        thermostats[thermostatId].name = userData.name
                if userData.operatingMode:
                        thermostats[thermostatId].operatingMode = userData.operatingMode
                if userData.coolSetPoint:
                        thermostats[thermostatId].coolSetPoint = userData.coolSetPoint
                if userData.heatSetPoint:
                        thermostats[thermostatId].heatSetPoint = userData.heatSetPoint
                if userData.fanMode:
                        thermostats[thermostatId].fanMode = userData.fanMode
                if userData.name:
                        thermostats[thermostatId].name = userData.name
                        
                return json.dumps(thermostats[thermostatId].to_JSON())
        

if __name__ == "__main__":
    app.run()

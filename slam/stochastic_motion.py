# --------------
# USER INSTRUCTIONS
#
# Write a function called stochastic_value that 
# returns two grids. The first grid, value, should 
# contain the computed value of each cell as shown 
# in the video. The second grid, policy, should 
# contain the optimum policy for each cell.
#
# --------------
# GRADING NOTES
#
# We will be calling your stochastic_value function
# with several different grids and different values
# of success_prob, collision_cost, and cost_step.
# In order to be marked correct, your function must
# RETURN (it does not have to print) two grids,
# value and policy.
#
# When grading your value grid, we will compare the
# value of each cell with the true value according
# to this model. If your answer for each cell
# is sufficiently close to the correct answer
# (within 0.001), you will be marked as correct.

delta = [[-1, 0 ], # go up
         [ 0, -1], # go left
         [ 1, 0 ], # go down
         [ 0, 1 ]] # go right

delta_name = ['^', '<', 'v', '>'] # Use these when creating your policy grid.

# ---------------------------------------------
#  Modify the function stochastic_value below
# ---------------------------------------------

def stochastic_value(grid,goal,cost_step,collision_cost,success_prob):
    failure_prob = (1.0 - success_prob)/2.0 # Probability(stepping left) = prob(stepping right) = failure_prob
    value = [[collision_cost for col in range(len(grid[0]))] for row in range(len(grid))]
    policy = [[' ' for col in range(len(grid[0]))] for row in range(len(grid))]

    change = True

    while change:
        change = False

        for x in range(len(grid)):
            for y in range(len(grid[0])):
                if goal[0] == x and goal[1] == y:
                    if value[x][y] > 0:
                        value[x][y] = 0
                        policy[x][y] = '*'

                        change = True

                elif grid[x][y] == 0:
                    for a in range(len(delta)):
                        x2 = x + delta[a][0]
                        y2 = y + delta[a][1]


                        if in_grid(x2, y2, grid):

                            left = [0,0] # x,y
                            right = [0,0] # x,y

                            # cost is = what's left * prob + what's right * prob + what's straight * success_prob
                            if delta[a] == delta[0]: # up
                                left = [x + delta[1][0], y + delta[1][1]]
                                right = [x + delta[3][0], y + delta[3][1]]
                            if delta[a] == delta[1]: # left
                                left = [x + delta[2][0], y + delta[2][1]]
                                right = [x + delta[0][0], y + delta[0][1]]
                            if delta[a] == delta[2]: # down
                                left = [x + delta[3][0], y + delta[3][1]]
                                right = [x + delta[1][0], y + delta[1][1]]
                            if delta[a] == delta[3]: # right
                                left = [x + delta[0][0], y + delta[0][1]]
                                right = [x + delta[2][0], y + delta[2][1]]

                            v_l = 0
                            v_r = 0

                            if in_grid(left[0], left[1], grid):
                                v_l = (value[left[0]][left[1]] + cost_step) * failure_prob
                            else:
                                v_l = (collision_cost + cost_step) * failure_prob

                            if in_grid(right[0], right[1], grid):
                                v_r = (value[right[0]][right[1]] + cost_step) * failure_prob
                            else:
                                v_r = (collision_cost + cost_step) * failure_prob

                            v2 = (value[x2][y2] + cost_step) * success_prob + v_l + v_r

                            if v2 < value[x][y]:
                                change = True
                                value[x][y] = v2

                                if delta[a][0] == -1 and delta[a][1] == 0: # up
                                    policy[x][y] = delta_name[0]
                                if delta[a][0] == 0 and delta[a][1] == -1: # left
                                    policy[x][y] = delta_name[1]
                                if delta[a][0] == 1 and delta[a][1] == 0: # down
                                    policy[x][y] = delta_name[2]
                                if delta[a][0] == 0 and delta[a][1] == 1: # right
                                    policy[x][y] = delta_name[3]

    return value, policy

def in_grid(x, y, grid):
    return x >= 0 and x < len(grid) and y >= 0 and y < len(grid[0]) and grid[x][y] == 0

# ---------------------------------------------
#  Use the code below to test your solution
# ---------------------------------------------

grid = [[0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 1, 1, 0]]
goal = [0, len(grid[0])-1] # Goal is in top right corner
cost_step = 1
collision_cost = 100
success_prob = 0.5

value,policy = stochastic_value(grid,goal,cost_step,collision_cost,success_prob)
for row in value:
    print row
for row in policy:
    print row

# Expected outputs:
#
# [57.9029, 40.2784, 26.0665,  0.0000]
# [47.0547, 36.5722, 29.9937, 27.2698]
# [53.1715, 42.0228, 37.7755, 45.0916]
# [77.5858, 100.00, 100.00, 73.5458]
#
# ['>', 'v', 'v', '*']
# ['>', '>', '^', '<']
# ['>', '^', '^', '<']
# ['^', ' ', ' ', '^']

# ----------
# User Instructions:
#
# Create a function compute_value which returns
# a grid of values. The value of a cell is the minimum
# number of moves required to get from the cell to the goal.
#
# If a cell is a wall or it is impossible to reach the goal from a cell,
# assign that cell a value of 99.
# ----------
import copy

grid = [[0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0]]
# grid = [[0, 1, 0, 1, 0, 0],
#         [0, 1, 0, 1, 0, 0],
#         [0, 1, 0, 1, 0, 0],
#         [0, 1, 0, 1, 0, 0],
#         [0, 0, 0, 1, 0, 0]]
goal = [len(grid)-1, len(grid[0])-1]
cost = 1 # the cost associated with moving from a cell to an adjacent one

delta = [[-1, 0 ], # go up
         [ 0, -1], # go left
         [ 1, 0 ], # go down
         [ 0, 1 ]] # go right

delta_name = ['^', '<', 'v', '>']

def compute_value(grid,goal,cost):
    # ----------------------------------------
    # insert code below
    # ----------------------------------------

    # make sure your function returns a grid of values as
    # demonstrated in the previous video.
    value = copy.deepcopy(grid)

    for y in range(len(value)):
        for x in range(len(value[y])):
            if value[y][x] == 1:
                value[y][x] = 99

    for y in range(len(value)):
        for x in range(len(value[y])):
            if value[y][x] == 0 and not(y == goal[0] and x == goal[1]):
                init = [x,y]
                path_cost = search(grid, init, goal, cost)
                value[y][x] = path_cost[0]

    return value

def search(grid,init,goal,cost):

    open_path = [[0, init[1], init[0], 0]]

    while True:

        location = min_step(open_path)

        if location == None:
            return [99]

        if location[1:3] == goal:
            return location[:3]

        for d in delta:

            next_y = location[1] + d[0]
            next_x = location[2] + d[1]
            next_step_good = next_step_valid(next_x, next_y, grid, open_path)

            if next_step_good:
                # add a potential next step
                # print "adding to open %d %d %d" % (location[0] + cost, next_y, next_x)
                open_path.append([location[0] + cost, next_y, next_x, 0])

    return None

def next_step_valid(next_x, next_y, grid, open_path):
    if step_in_grid(next_x, next_y, grid) and step_not_in_wall(next_x, next_y, grid) and step_not_taken(next_x, next_y, open_path):
        return True
    else:
        return False

def step_in_grid(next_x, next_y, grid):
    x_in_grid = (next_x >= 0) and (next_x < len(grid[0]))
    y_in_grid = (next_y >= 0) and (next_y < len(grid))
    return x_in_grid and y_in_grid

def step_not_in_wall(next_x, next_y, grid):
    return grid[next_y][next_x] != 1

def step_not_taken(next_x, next_y, open_path):
    for step in open_path:
        if (step[2] == next_x) and (step[1] == next_y):
            return False
    return True

# get the next available step with the smallest g-value
def min_step(open_path):

    optimal = None

    for p in open_path:
        if (optimal is None or p[0] < optimal[0]) and p[3] != 1:
            optimal = p
            optimal[3] = 1 # check it off
            break

    return optimal

cost_grid = compute_value(grid, goal, cost)
for i in range(len(cost_grid)):
    print cost_grid[i]

#print search(grid, [5,0], goal, cost)
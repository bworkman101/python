# ----------
# User Instructions:
#
# Implement the function optimum_policy2D below.
#
# You are given a car in grid with initial state
# init. Your task is to compute and return the car's
# optimal path to the position specified in goal;
# the costs for each motion are as defined in cost.
#
# There are four motion directions: up, left, down, and right.
# Increasing the index in this array corresponds to making a
# a left turn, and decreasing the index corresponds to making a
# right turn.

forward = [[-1,  0], # go up
           [ 0, -1], # go left
           [ 1,  0], # go down
           [ 0,  1]] # go right
forward_name = ['up', 'left', 'down', 'right']

# action has 3 values: right turn, no turn, left turn
action = [-1, 0, 1]
action_name = ['R', '#', 'L']

# EXAMPLE INPUTS:
# grid format:
#     0 = navigable space
#     1 = unnavigable space
# grid = [[1, 1, 1, 0, 0, 0],
#         [1, 1, 1, 0, 1, 0],
#         [0, 0, 0, 0, 0, 0],
#         [1, 1, 1, 0, 1, 1],
#         [1, 1, 1, 0, 1, 1]]
#
# init = [4, 3, 0] # given in the form [row,col,direction]
# # direction = 0: up
# #             1: left
# #             2: down
# #             3: right
#
# goal = [2, 0] # given in the form [row,col]
#
# cost = [2, 1, 20] # cost has 3 values, corresponding to making
# a right turn, no turn, and a left turn

grid = [[0, 0, 0, 0, 1, 1],
        [0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 1, 1, 1, 0],
        [0, 0, 0, 0, 1, 0]]

init = [4, 5, 0]
goal = [4, 3]
cost = [1, 1, 1]

# EXAMPLE OUTPUT:
# calling optimum_policy2D with the given parameters should return
# [[' ', ' ', ' ', 'R', '#', 'R'],
#  [' ', ' ', ' ', '#', ' ', '#'],
#  ['*', '#', '#', '#', '#', 'R'],
#  [' ', ' ', ' ', '#', ' ', ' '],
#  [' ', ' ', ' ', '#', ' ', ' ']]
# ----------

# ----------------------------------------
# modify code below
# ----------------------------------------

def optimum_policy2D(grid,init,goal,cost):

    policy2D = [[' ' for row in range(len(grid[0]))] for col in range(len(grid))]
    open_path = [[0, init[1], init[0]]]
    direction = forward[init[2]]
    location = open_path[0]

    while True:

        old_location = location
        open_path = min_step(open_path)
        location = open_path[0]
        open_path = []

        if location == None:
            return "fail"

        direction_shift = [location[2] - old_location[2], location[1] - old_location[1]]# as [y,x]
        (direction, direction_action) = determine_direction(direction, direction_shift)

        policy2D[old_location[2]][old_location[1]] = action_name[direction_action]

        if location[1] == goal[1] and location[2] == goal[0]:
            policy2D[location[2]][location[1]] = '*'
            return policy2D

        for d in forward:

            next_y = location[2] + d[0]
            next_x = location[1] + d[1]
            next_step_good = next_step_valid(next_x, next_y, grid)
            next_step_good = next_step_good and old_location[1:3] != [next_x, next_y]

            if next_step_good:
                # add a potential next step
                # print "adding to open %d %d %d" % (location[0] + cost, next_y, next_x)
                direction_shift = [next_y - location[2], next_x - location[1]]# as [y,x]
                (_, direction_action) = determine_direction(direction, direction_shift)
                open_path.insert(0, [location[0] + cost[direction_action], next_x, next_y])

def determine_direction(direction, direction_shift):
    direction_action = 1 # 2 left, 1 straight, 0 right

    if direction != direction_shift and direction_shift != [0,0]:
        if direction[1] == 0: # from vertical to horizontal
            direction_action = 2 if direction[0] == direction_shift[1] else 0  # the signs of y and x will match
        else: # from horizontal to vertical
            if direction == forward[1]:
                direction_action = 0 if direction_shift == forward[0] else 2
            else:
                direction_action = 2 if (direction == forward[3] and direction_shift == forward[0]) else 0

        return direction_shift, direction_action
    else:
        return direction, direction_action

def next_step_valid(next_x, next_y, grid):
    if step_in_grid(next_x, next_y, grid) and step_not_in_wall(next_x, next_y, grid):
        return True
    else:
        return False

def step_in_grid(next_x, next_y, grid):
    x_in_grid = (next_x >= 0) and (next_x < len(grid[0]))
    y_in_grid = (next_y >= 0) and (next_y < len(grid))
    return x_in_grid and y_in_grid

def step_not_in_wall(next_x, next_y, grid):
    return grid[next_y][next_x] != 1

# get the next available step with the smallest g-value
def min_step(open_path):

    optimal = None

    for p in open_path:
        if optimal is None or p[0] < optimal[0]:
            optimal = p

    return [optimal]

for i in range(len(grid)):
    print grid[i]

cost_grid = optimum_policy2D(grid, init, goal, cost)
for i in range(len(cost_grid)):
    print cost_grid[i]
# ----------
# User Instructions:
# 
# Define a function, search() that returns a list
# in the form of [optimal path length, row, col]. For
# the grid shown below, your function should output
# [11, 4, 5].
#
# If there is no valid path from the start point
# to the goal, your function should return the string
# 'fail'
# ----------

# Grid format:
#   0 = Navigable space
#   1 = Occupied space

grid = [[0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 1, 1, 1, 0],
        [0, 0, 0, 0, 1, 0]]
init = [0, 0]
goal = [len(grid)-1, len(grid[0])-1]
cost = 1

delta = [[-1, 0], # go up
         [ 0,-1], # go left
         [ 1, 0], # go down
         [ 0, 1]] # go right

delta_name = ['^', '<', 'v', '>']

def search(grid,init,goal,cost):

    open_path = [[0, init[1], init[0], 0]]

    while True:

        location = min_step(open_path)

        if location == None:
            return "fail"

        if location[1:3] == goal:
            return location[:3]
        
        for d in delta:

            next_y = location[1] + d[0]
            next_x = location[2] + d[1]
            next_step_good = next_step_valid(next_x, next_y, grid, open_path)
            
            if next_step_good:
                # add a potential next step
                # print "adding to open %d %d %d" % (location[0] + cost, next_y, next_x)
                open_path.append([location[0] + cost, next_y, next_x, 0])

    return None

def next_step_valid(next_x, next_y, grid, open_path):
    if step_in_grid(next_x, next_y, grid) and step_not_in_wall(next_x, next_y, grid) and step_not_taken(next_x, next_y, open_path):
        return True
    else:
        return False

def step_in_grid(next_x, next_y, grid):
    x_in_grid = (next_x >= 0) and (next_x < len(grid[0]))
    y_in_grid = (next_y >= 0) and (next_y < len(grid))
    return x_in_grid and y_in_grid

def step_not_in_wall(next_x, next_y, grid):
    return grid[next_y][next_x] != 1

def step_not_taken(next_x, next_y, open_path):
    for step in open_path:
        if (step[2] == next_x) and (step[1] == next_y):
            return False
    return True

# get the next available step with the smallest g-value
def min_step(open_path):

    optimal = None
    
    for p in open_path:
        if (optimal is None or p[0] < optimal[0]) and p[3] != 1:
            optimal = p
            optimal[3] = 1 # check it off
            break

    return optimal
        

print search(grid, init, goal, cost)


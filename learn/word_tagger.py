import nltk

# text = nltk.word_tokenize("And now for something completely different")
# print nltk.pos_tag(text)
#
nltk.help.upenn_tagset('VBN')
#
# nltk.corpus.brown.readme()
#
# from nltk.corpus import brown
# brown_news_tagged = brown.tagged_words(categories='news', tagset='universal')
# tag_fd = nltk.FreqDist(tag for (word, tag) in brown_news_tagged)
# print tag_fd.most_common()
# tag_fd.plot(cumulative=True)

wsj = nltk.corpus.treebank.tagged_words()
cfd2 = nltk.ConditionalFreqDist((tag, word) for (word, tag) in wsj)
past_participles = list(cfd2['VBN'])

f = open('/home/ben/Desktop/preceding_past_participles.txt', 'w')

for pp in past_participles:
    idx_of_pp = wsj.index((pp, 'VBN'))
    word_tag = wsj[idx_of_pp-1:idx_of_pp-1+1]
    preceding_past_participle = word_tag[0][0] + "," + word_tag[0][1] + " -> " + pp + "\n"
    f.write(preceding_past_participle)
    print preceding_past_participle

f.close()

# nltk.chat.chatbots()
import nltk
from nltk.corpus import names

def v1():

    def gender_features(word):
        return {'last_letter': word[-1], 'word_length': len(word)}

    print "get features from Shrek", gender_features('Shrek')

    labeled_names = ([(name, 'male') for name in names.words('male.txt')] + [(name, 'female') for name in names.words('female.txt')])
    print "number of labeled names", len(labeled_names)

    import random
    random.shuffle(labeled_names)

    featuresets = [(gender_features(n), gender) for (n, gender) in labeled_names]
    train_set, test_set = featuresets[500:], featuresets[:500]
    classifier = nltk.NaiveBayesClassifier.train(train_set)

    def classify_name(name):
        return "classify " + name + " " + classifier.classify(gender_features(name))

    print classify_name("Neo")
    print classify_name("Trinity")
    print classify_name("Andy")
    print classify_name("Pat")
    print classify_name("Zelda")
    print classify_name("jose")

    print "classifier accuracy", nltk.classify.accuracy(classifier, test_set)

    print classifier.show_most_informative_features(24)

def v2():

    def gender_features(word):
        return {'last_letter': word[-1], 'word_length': len(word)}

    # def gender_features(word):
    #     return {'suffix1': word[-1], 'suffix2': word[-2]}

    print "get features from Shrek", gender_features('Shrek')

    labeled_names = ([(name, 'male') for name in names.words('male.txt')] + [(name, 'female') for name in names.words('female.txt')])
    print "number of labeled names", len(labeled_names)

    import random
    random.shuffle(labeled_names)

    train_names = labeled_names[1500:]
    devtest_names = labeled_names[500:1500]
    test_names = labeled_names[:500]

    train_set = [(gender_features(n), gender) for (n, gender) in train_names]
    devtest_set = [(gender_features(n), gender) for (n, gender) in devtest_names]
    test_set = [(gender_features(n), gender) for (n, gender) in test_names]

    classifier = nltk.NaiveBayesClassifier.train(train_set)
    print(nltk.classify.accuracy(classifier, devtest_set)) # what's my accuracy

    # show me the errors
    errors = []
    for (name, tag) in devtest_names:
        guess = classifier.classify(gender_features(name))
        if guess != tag:
            errors.append( (tag, guess, name) )

    for (tag, guess, name) in sorted(errors):
        print('correct={:<8} guess={:<8s} name={:<30}'.format(tag, guess, name))

    # def classify_name(name):
    #     return "classify " + name + " " + classifier.classify(gender_features(name))
    #
    # print classify_name("Neo")
    # print classify_name("Trinity")
    # print classify_name("Andy")
    # print classify_name("Pat")
    # print classify_name("Zelda")
    # print classify_name("jose")
    #
    # print "classifier accuracy", nltk.classify.accuracy(classifier, test_set)
    #
    # print classifier.show_most_informative_features(24)

v2()
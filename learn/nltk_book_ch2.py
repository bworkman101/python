__author__ = 'ben'

import nltk

# 2)
# Use the corpus module to explore austen-persuasion.txt.
# How many word tokens does this book have? How many word types?
def _2():
    words = nltk.corpus.gutenberg.words('austen-persuasion.txt')
    print "word tokens %d word types %d" % (len(words), len(set(words)))

# 3)
# Use the Brown corpus reader nltk.corpus.brown.words() or the Web text corpus reader
# nltk.corpus.webtext.words() to access some sample text in two different genres.
def _3():
    print nltk.corpus.brown.categories()
    print nltk.corpus.brown.words(categories='humor')
    print nltk.corpus.brown.words(categories='fiction')

# 4)
# Read in the texts of the State of the Union addresses, using the state_union corpus reader.
# Count occurrences of men, women, and people in each document.
# What has happened to the usage of these words over time?
def _4():
    cfd = nltk.ConditionalFreqDist(
            (target, fileid[:4])
            for fileid in nltk.corpus.state_union.fileids()
            for w in nltk.corpus.state_union.words(fileid)
            for target in ['men', 'women', 'people']
            if w.encode('utf-8','ignore').lower().startswith(target))
    cfd.plot()

# Investigate the holonym-meronym relations for some nouns.
# Remember that there are three kinds of holonym-meronym relation, so you need to use: member_meronyms(),
# part_meronyms(), substance_meronyms(), member_holonyms(), part_holonyms(), and substance_holonyms().
def _5():
    from nltk.corpus import wordnet as wn
    print wn.synsets("human")
    for synset in wn.synset("man.n.01"), wn.synset("woman.n.01"), wn.synset('human.a.01'), wn.synset('body.n.01'):
        print "synset name ", synset.name()
        print "       definition ", synset.definition()
        print "   member_meronyms ", synset.member_meronyms()
        print "   part_meronyms ", synset.part_meronyms()
        print "   substance_meronyms ", synset.substance_meronyms()
        print "   member_holonyms ", synset.member_holonyms()
        print "   part_holonyms ", synset.part_holonyms()
        print "   substance_holonyms ", synset.substance_holonyms()

    man = wn.synset("man.n.01")
    woman = wn.synset("woman.n.01")
    human = wn.synset("human.n.01")
    print "man to woman path_similarity", man.path_similarity(woman)
    print "man to human path_similarity", man.path_similarity(human)

# In the discussion of comparative wordlists, we created an object called translate which you could
# look up using words in both German and Spanish in order to get corresponding words in English.
# What problem might arise with this approach? Can you suggest a way to avoid this problem?
def _6():
    from nltk.corpus import swadesh
    print swadesh.fileids()
    en2de = swadesh.entries(['en', 'de'])
    print en2de
    translate = dict(en2de)
    print translate['']

# According to Strunk and White's Elements of Style, the word however, used at the start of a sentence,
# means "in whatever way" or "to whatever extent", and not "nevertheless".
# They give this example of correct usage: However you advise him, he will probably do as he thinks best.
# (http://www.bartleby.com/141/strunk3.html) Use the concordance tool to study actual usage of this word
# in the various texts we have been considering. See also the LanguageLog posting "Fossilized prejudices
# about 'however'" at http://itre.cis.upenn.edu/~myl/languagelog/archives/001913.html
def _7():
    emma = nltk.Text(nltk.corpus.gutenberg.words('austen-emma.txt'))
    print emma.concordance('however')
    alice = nltk.Text(nltk.corpus.gutenberg.words('carroll-alice.txt'))
    print alice.concordance('however')

print len(nltk.corpus.gutenberg.fileids())

lesson = [_2, _3, _4, _5, _6, _7]
lesson[0]()

#_2()
#_3()
#_4()
#_5()
#_6()
#_7()

# news_rom = [(category, target)
#             for category in nltk.corpus.brown.categories()
#             if category in ['news', 'romance']
#             for word in nltk.corpus.brown.words(categories=category)
#             for target in ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']
#             if word.lower().startswith(target)]
# news_rom_freq = nltk.ConditionalFreqDist(news_rom)
# news_rom_freq.plot()

# news_rom = [(category, target)
#             for category in nltk.corpus.brown.categories()
#             if category in ['news', 'romance']
#             for word in nltk.corpus.brown.words(categories=category)
#             for target in ['man', 'woman']
#             if word.lower().startswith(target)]
# news_rom_freq = nltk.ConditionalFreqDist(news_rom)
# news_rom_freq.plot()
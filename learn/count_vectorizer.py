__author__ = 'ben'

import numpy as np
from sklearn.feature_extraction.text import CountVectorizer

vectorizer = CountVectorizer()

v_fit = vectorizer.fit(["hello world this is the world", "welcome to my world"])

v_transform = v_fit.transform(["welcome to my world"])

print v_fit.get_feature_names()
print v_transform.shape
print zip(v_fit.get_feature_names(), np.asarray(v_transform.sum(axis=0)).ravel())
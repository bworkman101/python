add JAR hdfs:///user/hdfs/serdejars/json-serde-1.3.8-SNAPSHOT-jar-with-dependencies.jar;
use bianalytics;

select 
regexp_replace(regexp_replace(regexp_replace(data.comment, '\\n|\\r|\\t', ' '), '\'', '\\\\\''), '\"', '\\\\\"') as comment
,from_unixtime(cast(substr(eventdate,0,10) as int)) as eventdate 
from ugc
where 
eventid is not null and data.comment is not null;

import gzip
import simplejson as json

class GzItr(object):

    def __init__(self,gz):
        self.gz = gz

    def __iter__(self):
        return self

    def next(self):
        line = gz.next()
        obj = json.loads(line)
        print obj['title']
        return obj['content']

gz = gzip.open('/home/ben/Documents/datasets/signalmedia-1m.jsonl.gz', 'r')

idx = 0;

for article in GzItr(gz):
    doc_name = "/home/ben/Documents/datasets/signalmedia-1m/%i.txt" % idx
    f = open(doc_name, 'write')
    f.write(article.encode("utf-8"))
    idx += 1
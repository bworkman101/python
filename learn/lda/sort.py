import gzip
import simplejson as json

class GzItr(object):

    def __init__(self,gz):
        self.gz = gz

    def __iter__(self):
        return self

    def next(self):
        next_line = self.gz.next()
        obj = json.loads(next_line)
        # print obj['media-type'], obj['title']
        return obj['title']

if __name__ == "__main__":

    gz = gzip.open('/home/ben/Documents/datasets/signalmedia-1m.jsonl.bigger.gz', 'r')

    titles = []
    for title in GzItr(gz):
        titles.append(title)

    model_file = open('/home/ben/Documents/datasets/predictions.dat', 'r')

    topics = {}

    doc_id = 1

    for line in model_file:
        weights = line.replace(" \n", "").split(" ")
        weights = [float(w) for w in weights[1:]]
        topic = 0
        max_weight = 0
        for idx in range(len(weights)):
            w = weights[idx]
            if w > max_weight:
                max_weight = w
                topic = idx

        if topic not in topics:
            topics[topic] = []
        topics[topic].append([doc_id, max_weight])
        doc_id += 1

    for topic in topics:
        topics[topic] = sorted(topics[topic], key=lambda entry: entry[1], reverse=True)

    by_topic = open('/home/ben/Documents/datasets/by_topic.bigger.txt', 'w')

    for topic, docs in topics.iteritems():
        by_topic.write("topic: %d\n" % topic)
        for doc in docs:
            title = titles[doc[0] - 1].encode('ascii', 'ignore').decode('ascii')
            by_topic.write("%-8s -> %-15f %s\n" % (doc[0], doc[1], title))
        by_topic.write("\n\n")

    model_file.close()
    by_topic.close()

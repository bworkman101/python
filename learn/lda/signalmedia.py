import gzip
import codecs
import simplejson as json
import nltk

lemmatizer = nltk.stem.WordNetLemmatizer()
stemmer = nltk.stem.snowball.SnowballStemmer("english")

class GzItr(object):

    def __init__(self, gz):
        self.gz = gz

    def __iter__(self):
        return self

    def next(self):
        line = gz.next()
        obj = json.loads(line)
        # print obj['media-type'], obj['title']
        return obj['content']

def save_dictionary(dictionary):
    words_file = codecs.open('/home/ben/Documents/datasets/word_index.txt', 'w', 'utf-8-sig')
    for key, value in dictionary.iteritems():
        words_file.write(key + "=" + `value` + "\n")
    words_file.close()

def index_words(dic, words):
    indexes = []
    for word in words:
        if word in dic:
            indexes.append(dic[word])
        else:
            new_idx = len(dic)
            dic[word] = new_idx
            indexes.append(new_idx)
    return indexes

# gz = gzip.open('/home/ben/Documents/datasets/signalmedia-1m.jsonl.gz', 'r')
gz = gzip.open('/home/ben/Documents/datasets/signalmedia-1m.jsonl.bigger.gz', 'r')
# gz = gzip.open('/home/ben/Documents/datasets/signalmedia-1m.jsonl.sample.gz', 'r')

# file_out = codecs.open('/home/ben/Documents/datasets/all_content.txt', 'w', 'utf-8-sig')
file_out = codecs.open('/home/ben/Documents/datasets/bigger_content.txt', 'w', 'utf-8')
# file_out = codecs.open('/home/ben/Documents/datasets/sample_content.txt', 'w', 'utf-8-sig')

counter = 0

for content in GzItr(gz):
    if counter % 100 == 0:
        print counter, "written"
    # indexed = index_words(dictionary, words)
    clean_content = content
    file_out.write(clean_content.replace("\n", ""))
    counter += 1

# save_dictionary(dictionary)

print counter, "written"

file_out.close()
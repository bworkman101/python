import codecs
import nltk
import gzip
import simplejson as json

lemmatizer = nltk.stem.WordNetLemmatizer()

class GzItr(object):

    def __init__(self, gz):
        self.gz = gz

    def __iter__(self):
        return self

    def next(self):
        line = gz.next()
        obj = json.loads(line)
        # print obj['media-type'], obj['title']
        return obj['content']

def load_dictionary(name):
    words_file = codecs.open("/home/ben/Documents/datasets/%s" % name, 'r', 'utf-8-sig')
    dic = {}
    for line in words_file:
        entry = line.replace("\n", "").split("=")
        try:
            dic[entry[0]] = int(entry[1])
        except ValueError:
            print "bad"

    words_file.close()
    return dic

def clean_line(line):
    line = line.lower()
    line = line.replace("\n", " ").replace("\r", " ").replace(":", "").replace("=", "")
    line = nltk.word_tokenize(line)
    line = [lemmatizer.lemmatize(w) for w in line]
    line = [w.encode('ascii', 'ignore').decode('ascii') for w in line]
    # line = [stemmer.stem(w.encode('ascii', 'ignore').decode('ascii')) for w in line]
    return line

def index_words(dic, words):
    indexes = []
    for word in words:
        if word in dic:
            indexes.append(dic[word])
        else:
            new_idx = len(dic) + 1
            dic[word] = new_idx
            indexes.append(new_idx)
    return indexes

def save_dictionary(dictionary, name):
    words_file = codecs.open("/home/ben/Documents/datasets/%s" % name, 'w', 'utf-8-sig')
    for key, value in dictionary.iteritems():
        words_file.write("%s=%d\n" % (key, value))
    words_file.close()

if __name__ == "__main__":

    file = "signalmedia-1m.jsonl.bigger"
    gz = gzip.open("/home/ben/Documents/datasets/%s.gz" % file, 'r')
    file_out = codecs.open("/home/ben/Documents/datasets/%s.idx" % file, 'w', 'utf-8-sig')

    dictionary = load_dictionary("word_index.bigger")

    for content in GzItr(gz):
        words = clean_line(content)
        indexed = index_words(dictionary, words)
        line = ""
        for ind in indexed:
            line += " " + str(ind)
        file_out.write(line + "\n")

    save_dictionary(dictionary, "word_index.bigger")

    gz.close()
    file_out.close()
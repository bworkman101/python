__author__ = 'ben'

import math
import nltk
import os
import re
import sys
import string
from collections import Counter
from nltk import tokenize
from nltk import word_tokenize
from nltk.stem.snowball import SnowballStemmer
stemmer = SnowballStemmer("english")
remove_words = ['the', 'and', 'is', 'are', 'a', 'an', 'nntppostinghost', 'maxaxaxaxaxaxaxaxaxaxaxaxaxaxax',
                'stephanopoulo', 'composmswindowsmisc', 'compsysmachardwar', 'sandviknewtonapplecom',
                'accessdigexnet', 'serazumauucp', 'composmswindowsapp',
                'that', 'for', 'you', 'this', 'not', 'have', 'with', 'but', 'they', 'from', 'can', 'what',
                'there', 'all', 'will', 'would', 'your', 'were', 'their', 'scispac', 'his', 'who', 'scsi'
                ]
pattern_1 = re.compile(".*edu")  # remove email ids
def remove_punctuation(s):
    for c in string.punctuation:
        s = s.replace(c, "")
    return s

def read_news_file(fname):
    sent = ''
    '''
    There are few lines in the beginning of each mail that we need to ignore
    '''
    words = []
    with open(fname) as f:
        for line in f:
            sent += line

    sent = sent.lower()
    sent = remove_punctuation(sent)
    sent = sent.translate(None, string.digits)
    tokens = word_tokenize(sent)
    tokens_1 = [w for w in tokens if '@' not in w]  # get rid of emails
    tokens_2 = [nltk.stem.WordNetLemmatizer().lemmatize(w) for w in tokens_1]
    tokens_3 = [stemmer.stem(w.decode('ascii', 'ignore')) for w in tokens_2]
    words.extend(tokens_3)
    words1 = [w for w in words if len(w) > 2 and w not in remove_words]
    words = [w for w in words1 if pattern_1.match(w) == None]
    return words

if __name__ == "__main__":

    data_dir = '20_newsgroups'  # 128,606 unique words (&gt;1), 127,965 (&gt;2)
    max_words = 65000
    min_count = 5
    vocab_file = 'newsgroup_selected_words.dat'
    out_file = 'newsgroups_lda_vw.data'
    vocab = set()
    word_counter = Counter()
    for root, dirnames, filenames in os.walk(data_dir):
        for filename in filenames:
            full_file_path = root + '/' + filename
            words = read_news_file(full_file_path)
            vocab = vocab.union(set(words))
            word_counter.update(words)
        print len(vocab)
    print len(word_counter)

    top_words_and_counts = word_counter.most_common(max_words)
    top_words = [e[0] for e in top_words_and_counts if e[1] >= min_count]  # greater than min_count
    fv = open(vocab_file, 'w')
    count_word = 0
    for w in top_words:
        out_str = str(count_word) + '\t' + w + '\t' + str(word_counter[w]) + '\n'
        fv.write(out_str)
        count_word += 1
    fv.close()
    print 'Selected {0} words in the vocabulary'.format(len(top_words))

    fw = open(out_file, 'w')
    count_file = 0
    for root, dirnames, filenames in os.walk(data_dir):
        print root
        for filename in filenames:
            full_file_path = root + '/' + filename
            words = read_news_file(full_file_path)
            filtered_words = [w for w in words if w in top_words]
            out_str = '| '
            for ii in range(len(top_words)):
                if words.count(top_words[ii]) > 0:
                    out_str += str(ii) + ':' + str(words.count(top_words[ii])) + ' '
            fw.write(out_str + '\n')
            count_file += 1
    fw.close()
    print '{0} lines are written in {1}'.format(count_file, out_file)



# Get the indices of top n words
def top_indices(x, n):
    xs = sorted(x, reverse=True)
    indices = []
    for e in xs[:n]:
        indices.append(x.index(e))
    return indices

if __name__ == "__main__":

    vocab_file = 'newsgroup_selected_words.dat'
    vw_out_file = 'topics.dat'
    out_file = 'topic_words.dat'
    num_topics = 10
    num_top_words = 10

    vocab_words = []
    with open(vocab_file) as f:
        for line in f:
            w = line.strip().split('\t')
            vocab_words.append(w[1])

    topics = [[] for w in range(num_topics)]
    with open(vw_out_file) as f:
        for _ in xrange(8):
            next(f)
        for line in f:
            w = line.strip().split(' ')[1:]
            for t in range(num_topics):
                topics[t].append(float(w[t]))

    header = '\t'.join(['Topic-'+str(i) for i in range(num_topics)])
    fw = open(out_file, 'w')
    fw.write(header+'\n')
    topics_words = []
    for t in range(num_topics):
        top_words = [vocab_words[i] for i in top_indices(topics[t], num_top_words)]
        topics_words.append(top_words)
    for t in range(num_top_words):
        out_str = '\t'.join([topics_words[w][t] for w in range(num_topics)])
        fw.write(out_str + '\n')
    fw.close()
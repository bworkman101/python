import numpy as np
import codecs
import lda

# load 2x2 ndarray from signalmedia-1m.jsonl.bigger.idx

if __name__ == "__main__":

    file = "signalmedia-1m.jsonl.bigger"
    documents = codecs.open("/home/ben/Documents/datasets/signalmedia-1m.jsonl.bigger.idx", 'r', 'utf-8-sig')

    docs = []

    max_len = 0

    for doc in documents:
        doc = doc.replace("\n", "").split(" ")[1:]
        docs.append([int(w) for w in doc])
        if max_len < len(doc):
            max_len = len(doc)

    docs = [ np.pad(doc, (0, max_len - len(doc)), 'constant', constant_values=(0)) for doc in docs]
    docs = np.array(docs)

    docs = np.array(docs)
    print docs.shape

    model = lda.LDA(n_topics=20, n_iter=500, random_state=1)
    model.fit(docs)

    print model

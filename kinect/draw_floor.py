import cv2
import numpy as np

def main():

    # init openc window
    windowName = "Floor"
    cv2.namedWindow(windowName)
    img = np.zeros((800,800,3), dtype=np.uint8)
    img += 255

    # read 2d data
    verts = np.load("/home/ben/development/python/kinect/local/3d.npy")
    xy = np.delete(verts, np.s_[1], 1)
    print xy.shape
    xy = xy[np.logical_and(xy[:,0] != 0, xy[:,1] != 0)]
    xy[:,0] = xy[:,0] * 180 + 400
    xy[:,1] = xy[:,1] * -180 + 800
    xy = xy.astype(int)

    for xi in range(xy.shape[0]):
        img[xy[xi,1], xy[xi,0], :] = 0
        # g.Point(xy[xi,0], xy[xi,1]).draw(win)

    cv2.imshow(windowName, img)

    print "done"

    cv2.waitKey(0)

main()

import numpy as np
import cv2
import sys

def trim3d(xyz):
    return xyz[np.logical_and(
            np.logical_and(
                np.logical_and(xyz[:,:,1] < .2, xyz[:,:,1] > -.2),
                np.logical_and(xyz[:,:,0] < 3, xyz[:,:,0] > -3)),
            np.logical_and(xyz[:,:,2] < 3, xyz[:,:,2] > -3))]

def fillFloorImg(xyz, img):
    img.fill(255)
    xy = np.delete(xyz, np.s_[1], 1)
    xy = xy[np.logical_and(xy[:,0] != 0, xy[:,1] != 0)]
    xy[:,0] = xy[:,0] * 180 + 400
    xy[:,1] = xy[:,1] * -180 + 800
    xy = xy.astype(int)

    for xi in range(xy.shape[0]):
        img[xy[xi,1], xy[xi,0], :] = 0

    return img

def main():
    floorWindow = "Floor"
    cv2.namedWindow(floorWindow)
    img = np.zeros((800,800,3), dtype=np.uint8)
    cv2.imshow(floorWindow, img)
    
    capture = cv2.VideoCapture()
    capture.open(cv2.CAP_OPENNI)

    if capture.isOpened() is False:
        print "Error: Cannot open video stream from camera\n"
        sys.exit(1)

    xyz = []
    bgr = []

    while (True):
        capture.grab()
        (retValXyz, xyz) = capture.retrieve(0, cv2.CAP_OPENNI_POINT_CLOUD_MAP)
        xyz = trim3d(xyz)
        img = fillFloorImg(xyz, img)
        cv2.imshow(floorWindow, img)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    
    # np.save("local/3d.npy", xyz)
    # (retValBgr, bgr) = capture.retrieve(0, cv2.CAP_OPENNI_BGR_IMAGE)
    # cv2.imwrite( "/home/ben/Desktop/image.jpg", bgr )

    capture.release()
    sys.exit(0)

main()

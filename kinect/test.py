import numpy as np
import re

pattern = re.compile(r'\[(-?\d+\.?\d*), (-?\d+\.?\d*), (-?\d+\.?\d*)\] ')

xyz = np.zeros((480*640,3))

def parseLine(line):
    for (x, y, z) in re.findall(pattern, line):
        # print "x=%s, y=%s, z=%s" % (x, y, z)
        # verts.append((float(x), float(z), float(y)))
        xyz.itemset(float(x), 0)
        xyz.itemset(float(y), 1)
        xyz.itemset(float(z), 2)

xyz = np.reshape(xyz, (480,640,3))
lines = tuple(open("local/3d.txt", 'r'))

for line in lines:
    parseLine(line)

print xyz

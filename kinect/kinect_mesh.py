import bpy
import numpy as np
 
def createMesh(name, origin, verts):
    # Create mesh and object
    me = bpy.data.meshes.new(name+'Mesh')
    ob = bpy.data.objects.new(name, me)
    ob.location = origin
    ob.show_name = True
    
    # Link object to scene
    bpy.context.scene.objects.link(ob)
 
    # Create mesh from given verts, edges, faces. Either edges or
    # faces should be [], or you ask for problems
    me.from_pydata(verts, [], [])
 
    # Update mesh with new data
    me.update(calc_edges=False)
    return ob
 
def run(origin):
    verts1 = np.load("/home/ben/development/python/kinect/local/3d.npy")

    z = np.delete(verts1, np.s_[0,1], 1)
    xy = np.delete(verts1, np.s_[2], 1)
    verts1 = np.insert(xy, [1], z, axis=1)
    
    ob1 = createMesh('Scan', origin, verts1)
 
    # Move second object out of the way
    ob1.select = False
    bpy.ops.transform.translate(value=(0,2,0))
    return
 
if __name__ == "__main__":
    run((0,0,0))
